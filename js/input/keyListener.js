define(function ()
{

    var keyStates = {};
    var axisMap = {};
    var pressMap = {};

    window.addEventListener('keydown', function (event)
    {
//        console.log("down: " + event.keyCode);
        keyStates[event.keyCode] = true;
        resolveKey(event.keyCode);
    }, false);

    window.addEventListener('keyup', function (event)
    {
        keyStates[event.keyCode] = false;
        resolveKey(event.keyCode);
    }, false);

    function resolveKey(keyCode)
    {
        // send axis events
        var axisObject = axisMap[keyCode];
        if (axisObject)
        {
            var value = (keyStates[axisObject.negative] ? -1 : 0) + (keyStates[axisObject.positive] ? 1 : 0);
            axisObject.callback(value);
        }

        // send press events
        if (keyStates[keyCode] && pressMap[keyCode])
        {
            pressMap[keyCode]();
        }
    }

    return {
        axis: function(negative, positive, callback)
        {
            var axisObject = {negative: negative, positive: positive, callback: callback};
            axisMap[negative] = axisObject;
            axisMap[positive] = axisObject;
        },
        press: function(key, callback)
        {
            pressMap[key] = callback;
        },
        W: 87,
        A: 65,
        S: 83,
        D: 68,
        P: 80,
        COMMA: 188,
        PERIOD: 190
    };
});
