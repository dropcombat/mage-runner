define(["entity", "core/message", "render/sys/camera", "render/sys/render"],
    function (entity, message, camera, renderSystem)
    {
        // debug function, add 'clicked' behavior to entity component
        message.addListener("clicked", function ()
        {
            console.log("clicked");
        });

        var clickableEntities = {};

        document.getElementById("container").addEventListener("click", function (e)
        {
            var eId, targetClickableData, trigger = false, hIndex = -1;

            var x = camera.screenToWorldX((e.x || e.clientX || 0) / renderSystem.getScale());
            var y = camera.screenToWorldY((e.y || e.clientY || 0) / renderSystem.getScale());

            for (var entityId in clickableEntities)
            {
                var clickableData = clickableEntities[entityId];
                var deltaX = x - clickableData.position.x;
                var deltaY = y - clickableData.position.y;
                var maxDeltaX = clickableData.clickable.width / 2;
                var maxDeltaY = clickableData.clickable.height / 2;

                if (deltaX < 0)
                {
                    deltaX = -deltaX;
                }

                if (deltaY < 0)
                {
                    deltaY = -deltaY;
                }

                if (deltaX <= maxDeltaX && deltaY <= maxDeltaY)
                {
                    // check to see if the zIndex is greater than the highest zIndex
                    if (clickableData.clickable.zIndex > hIndex)
                    {
                        trigger = true;
                        // the new highest zIndex is this entities zIndex
                        hIndex = clickableData.clickable.zIndex;
                        targetClickableData = clickableData;
                        eId = entityId;
                    }
                }
            }
            // if we clicked on something that needs to be triggered
            if (trigger)
            {
                // trigger the entity with the highest zIndex, reset trigger and highest index
                message.queue({type: targetClickableData.clickable.message,
                data: targetClickableData.clickable.data});
            }

        }, false);

        entity.createSystem("clickHandler", ["position", "clickable"], function ()
        {
        }, function (entityId)
        {
            clickableEntities[entityId] = {
                position: entity.getComponentDataForEntity("position", entityId),
                clickable: entity.getComponentDataForEntity("clickable", entityId)
            };
        }, function (entityId)
        {
            delete clickableEntities[entityId];
        });
    });
