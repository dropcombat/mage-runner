define(["entity", "core/message"], function (entity, message)
{
    var cache = {};

    entity.createSystem("trigger", ["trigger"], function (trigger, timer, entityId)
    {
        var data = cache[entityId];

        for (var tripperId in data)
        {
            var state = data[tripperId];
            if (state.on)
            {
                if (trigger.on)
                {
                    message.queue({
                        type: trigger.on,
                        data: trigger.data,
                        id: entityId,
                        triggering: tripperId
                    });
                }
                state.on = false;
            }

            if (state.trip)
            {
                state.trip = false;
            }
            else
            {
                if (trigger.off)
                {
                    message.queue({
                        type: trigger.off,
                        data: trigger.data,
                        id: entityId,
                        triggering: tripperId
                    });
                }
                delete data[tripperId];
            }
        }
    }, function (entityId)
    {
        cache[entityId] = {};
    }, function (entityId)
    {
        var data = cache[entityId];

        for (var tripperId in data)
        {
            var trigger = entity.getComponentDataForEntity("trigger", entityId);
            if (trigger.off)
            {
                message.queue({
                    type: trigger.off,
                    data: trigger.data,
                    id: entityId,
                    triggering: tripperId
                });
            }
        }

        delete cache[entityId];
    });

    return {
        trip: function (entityId, tripperId)
        {
            var data = cache[entityId];
            if (data)
            {
                if (data[tripperId])
                {
                    data[tripperId].trip = true;
                }
                else
                {
                    data[tripperId] = {on: true, trip: true};
                }
            }
        }
    };
});
