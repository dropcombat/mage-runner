define(["entity", "core/message"], function (entity, message)
{
    entity.createSystem("countdown", ["countdown"], function (countdown, timer, entityId)
    {
        countdown.timer -= timer.delta;
        if (countdown.timer <= 0)
        {
            entity.removeComponentFromEntity("countdown", entityId);
            entity.refreshEntity(entityId);
            message.queue({type: countdown.message, id: entityId, data: countdown.data});
        }
    });
});
