define(["entity"], function (entity)
{
    entity.createComponent("countdown", "countdown component", "trigger", function ()
    {
        return {
            timer: 0,
            message: "",
            data: null
        };
    });
});
