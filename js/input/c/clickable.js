define(["entity"], function (entity)
{
    entity.createComponent("clickable", "can be clicked on", "clickable", function ()
    {
        return {
            width: 0,
            height: 0,
            message: "",
            data: null,
            zIndex: 1
        };
    });
});
