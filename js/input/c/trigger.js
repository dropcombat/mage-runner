define(["entity"], function (entity)
{
    entity.createComponent("trigger", "trigger component", "trigger", function ()
    {
        return {
            on: "",
            off: "",
            data: null
        };
    });
});
