define(function ()
{
    // defaulting to .ogg since the file size is smaller
    //noinspection SpellCheckingInspection
    var audioExtension = (new Audio()).canPlayType("audio/ogg; codecs=vorbis") ? ".ogg" : ".mp3";
    var sfxCache = {};
    var playSfx = true;
    var playBackground = true;
    var currentBackgroundMusic;
    var currentBackgroundMusicName;

    function getAudioObject(path)
    {
        return new Audio(path + audioExtension);
    }

    return {
        play: function (name, channel)
        {
            if (playSfx)
            {
                var channelOrDefault = channel || 0;
                this.preloadSfx(name, channelOrDefault);
                var sfx = sfxCache[name][channelOrDefault];
                if (sfx.paused)
                {
                    sfx.play();
                }
                else if (sfx.currentTime)
                {
                    //noinspection JSUnusedGlobalSymbols
                    sfx.currentTime = 0;
                }
            }
        },
        preloadSfx: function (name, channel)
        {
            var channelOrDefault = channel || 0;
            if (!sfxCache[name])
            {
                sfxCache[name] = [];
            }
            if (!sfxCache[name][channelOrDefault])
            {
                sfxCache[name][channelOrDefault] = getAudioObject("resources/s/" + name);
            }
        },
        background: function (name)
        {
            if (name != currentBackgroundMusicName)
            {
                if (currentBackgroundMusic)
                {
                    currentBackgroundMusic.pause();
                }
                // start background music
                var backgroundMusic = getAudioObject("resources/m/" + name);
                //noinspection JSUnusedGlobalSymbols
                backgroundMusic.loop = true;
                //noinspection JSUnusedGlobalSymbols
                backgroundMusic.volume = 0.5;
                if (playBackground)
                {
                    backgroundMusic.play();
                }
                currentBackgroundMusic = backgroundMusic;
                currentBackgroundMusicName = name;
            }
            return currentBackgroundMusic;
        },
        getAudioExtension: function ()
        {
            return audioExtension;
        },
        toggleBackground: function ()
        {
            playBackground = !playBackground;
            if (playBackground)
            {
                currentBackgroundMusic.play();
            }
            else
            {
                currentBackgroundMusic.pause();
            }
        },
        getPlayBackground: function ()
        {
            return playBackground;
        },
        getPlaySfx: function ()
        {
            return playSfx;
        },
        setPlaySfx: function (state)
        {
            playSfx = state;
        }
    };
});