define(["entity", "core/message", "audio/util/sound"], function (entity, message, soundUtil)
{
    entity.createComponent("sounds", "mapping of sound types", "sounds", function ()
    {
        return {};
    });

    message.addListener("sound", function (msg)
    {
        var soundData = entity.getComponentDataForEntity("sounds", msg.id);
        if (soundData && soundData[msg.sound])
        {
            soundUtil.play(soundData[msg.sound]);
        }
    });

});
