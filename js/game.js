require(["core/gameLoop", "entity", "render/sys/render", "game/levelCollection", "audio/util/sound", "game/menu",
         "core/preload", "includes", "domReady!"],
    function (gameLoop, entity, renderSystem, levelCollection, soundUtil, menuHelper, preload)
    {
//    levelCollection.debugLevelSelection();
//    levelCollection.loadDefault();

        // create camera
        // positioned that a hero sized image placed at 0, 0 is the left edge, exactly in the middle
        var cameraEntity = entity.createEntity("camera");
        entity.addComponentToEntity("cameraFocus", cameraEntity);
        entity.addComponentToEntity("position", cameraEntity);
        var data = entity.getComponentDataForEntity("position", cameraEntity);
        data.x = 915;
        data.y = 0;
        entity.refreshEntity(cameraEntity);

        // on load up, show the main menu first.
        menuHelper.mainMenu();

        renderSystem.init(1920, 1080);

        gameLoop.setUpdateSystems(
            ["director", "powerGrabberTimeout", "seek", "patrol", "charge", "movement", "restrictedPosition", "orbit",
             "rangedAttacks", "collisionDetection", "trigger", "countdown", "death", "killWall", "hud", "centerCamera",
             "animation", "tileMovement", "save"]);
        gameLoop.setRenderSystems(["render"]);

        //noinspection SpellCheckingInspection
        preload.image([
            "arrow",
            "eye",
            "fireball",
            "flying bomb explosion",
            "flying bomb trigger",
            "flying bomb",
            "goblin archer",
            "goblin charger",
            "golem caster",
            "golem",
            "hero",
            "logo",
            "magerunner",
            "orb green",
            "orb",
            "pit",
            "runes explosion",
            "runes trigger",
            "runes",
            "small eye",
            "wall",
            "wizard",
            "powerups/bomb",
            "powerups/gold",
            "powerups/health",
            "powerups/multi",
            "powerups/speed",
            "tiles/cotton-candy",
            "tiles/dark-stone",
            "tiles/desert",
            "tiles/forest",
            "tiles/grass",
            "tiles/stone"
        ]);

        preload.sfx([
            "bomb",
            "coin",
            "explosion",
            "fireball",
            "orb",
            "powerup"
        ]);

        //noinspection SpellCheckingInspection
        preload.music([
            "DST-ClickThenAction",
            "DST-MirrorMix",
            "DST-RockitFlarex",
            "DST-NoReturn"
        ]);

        preload.addFilter("image", function(name)
        {
            return "resources/i/" + name + ".png";
        });

        preload.addFilter("sfx", function(name)
        {
            return "resources/s/" + name + soundUtil.getAudioExtension();
        });

        preload.addFilter("music", function(name)
        {
            return "resources/m/" + name + soundUtil.getAudioExtension();
        });

        preload.run(6, function()
        {
            levelCollection.startMusic();
            document.getElementById("loading").style.display = "none";
//            soundUtil.background("Ambush");
            gameLoop.start();
        });


    });
