define(["game/hero", "game/writer"], function (heroHelper, writer)
{
    function spawnEyes(seconds, y, count)
    {
        for (var ndx = 0; ndx < count; ndx++)
        {
            writer.writeEvent(seconds, "spawn", ["eye", y, ndx / count]);
        }
    }

    return {
        name: "Dungeon",
        music: "DST-NoReturn",
        setup: function ()
        {
            heroHelper.spawn();

            writer.newLevel({tile: "desert"});

            writer.section();
            writer.multiEvent({time: 0, count: 3, delta: 100}, "spawn", ["wall", -300]);
            writer.multiEvent({time: 0, count: 3, delta: 100}, "spawn", ["wall", 300]);
            writer.writeEvent(1, "spawn", ["rune", -300]);
            writer.writeEvent(1, "spawn", ["rune", 300]);
            writer.writeEvent(1.5, "spawn", ["golem", 100]);
            writer.writeEvent(1.5, "spawn", ["golem", -100]);
            writer.writeEvent(1.7, "spawn", ["archer", 100]);
            writer.writeEvent(1.7, "spawn", ["archer", -100]);

            writer.section(1);
            writer.repeatEvent({startTime: 2, duration: 1, stepTime: 1, stepY: 100}, "spawn", ["flying bomb", -300]);
            writer.repeatEvent({startTime: 3, duration: 4, stepTime: 2, stepY: 0}, "spawn", ["wizard", 400]);
            writer.repeatEvent({startTime: 3, duration: 4, stepTime: 2, stepY: 0}, "spawn", ["wizard", -400]);
            writer.writeEvent(6, "powerup", ["health", 0, 5000]);
            writer.repeatEvent({startTime: 9, duration: 4, stepTime: 2, stepY: 0}, "spawn", ["wall", 400]);
            writer.repeatEvent({startTime: 9, duration: 4, stepTime: 2, stepY: 0}, "spawn", ["wall", -400]);
            writer.multiEvent({time: 15, count: 3, delta: 100}, "spawn", ["wall", -300]);
            writer.multiEvent({time: 15, count: 3, delta: 100}, "spawn", ["wall", 300]);
            writer.writeEvent(16, "spawn", ["charger", 100]);
            writer.writeEvent(16, "spawn", ["charger", -100]);

            writer.section(2);
            writer.repeatEvent({startTime: 0, duration: 4, stepTime: 3, stepY: 0}, "spawn", ["wall", -400]);
            writer.repeatEvent({startTime: 1, duration: 1, stepTime: 1, stepY: 0}, "powerup", ["gold", -400]);
            writer.repeatEvent({startTime: 0, duration: 4, stepTime: 3, stepY: 0}, "spawn", ["wall", 400]);
            writer.repeatEvent({startTime: 1, duration: 1, stepTime: 1, stepY: 0}, "powerup", ["gold", 400]);
            writer.writeEvent(1.5, "spawn", ["pit", 300]);
            writer.writeEvent(1.5, "spawn", ["pit", -300]);
            writer.writeEvent(2.5, "spawn", ["wizard", 400]);
            writer.writeEvent(2.5, "spawn", ["wizard", -400]);

            writer.section(4);
            writer.writeEvent(0, "spawn", ["wall", -300]);
            writer.writeEvent(0, "spawn", ["wall", -400]);
            writer.writeEvent(0, "spawn", ["wall", 300]);
            writer.writeEvent(0, "spawn", ["wall", 400]);
            writer.writeEvent(0, "spawn", ["pit", 100]);
            writer.writeEvent(0, "spawn", ["pit", -100]);
            writer.writeEvent(0, "spawn", ["archer", 200]);
            writer.writeEvent(0, "spawn", ["archer", -200]);
            writer.writeEvent(1, "spawn", ["charger", 0]);
            writer.writeEvent(2, "spawn", ["archer", 100]);
            writer.writeEvent(2, "powerup", ["gold", 0]);
            writer.writeEvent(2, "spawn", ["archer", -100]);
            writer.writeEvent(3, "spawn", ["flying bomb", -400]);
            writer.writeEvent(3, "spawn", ["flying bomb", 400]);
            writer.writeEvent(6, "spawn", ["wizard", 100]);
            writer.writeEvent(6, "spawn", ["wizard", -100]);
            writer.writeEvent(6.5, "spawn", ["charger", 0]);

            writer.section(4);
            writer.writeEvent(0.5, "spawn", ["wizard", -300]);
            writer.multiEvent({time: 0, count: 6, delta: 100}, "spawn", ["wall", -150]);
            writer.repeatEvent({startTime: 0, duration: 6, stepTime: 0.5, stepY: 0}, "spawn", ["wall", 400]);
            writer.multiEvent({time: 6, count: 6, delta: 100}, "spawn", ["wall", 150]);
            writer.repeatEvent({startTime: 0, duration: 6, stepTime: 0.5, stepY: 0}, "spawn", ["wall", -400]);
            writer.writeEvent(1, "powerup", ["health", -100, 5000]);
            writer.multiEvent({time: 11, count: 4, delta: 100}, "spawn", ["wall", 0]);

            writer.section(4);
            writer.multiEvent({time: 0, count: 4, delta: 100}, "spawn", ["wall", -250]);
            writer.multiEvent({time: 0, count: 4, delta: 100}, "spawn", ["wall", 250]);
            writer.writeEvent(1, "spawn", ["rune", -150]);
            writer.writeEvent(1, "spawn", ["rune", 150]);
            writer.writeEvent(1, "spawn", ["wizard", 400]);
            writer.writeEvent(1, "spawn", ["wizard", -400]);
            writer.writeEvent(3, "spawn", ["wall", 100]);
            writer.writeEvent(3, "spawn", ["wall", 0]);
            writer.writeEvent(3, "spawn", ["wall", -100]);
            writer.writeEvent(3.5, "powerup", ["multi", 0, 3, 15]);
            writer.writeEvent(4.9, "spawn", ["golem", -200]);
            writer.writeEvent(4.9, "spawn", ["golem", 200]);
            writer.writeEvent(5, "spawn", ["golem", 0]);

            writer.section();
            spawnEyes(0, -300, 10);
            spawnEyes(0, 300, 10);
            writer.writeEvent(5, "spawn", ["golem caster", 0]);
            writer.writeEvent(5, "spawn", ["golem caster", 100]);
            writer.writeEvent(5, "spawn", ["golem caster", -100]);
            writer.writeEvent(7, "spawn", ["charger", -100]);
            writer.writeEvent(7, "spawn", ["charger", 0]);
            writer.writeEvent(7, "spawn", ["charger", 100]);
            writer.writeEvent(7.5, "spawn", ["wizard", 200]);
            writer.writeEvent(8, "spawn", ["wizard", 100]);
            writer.writeEvent(8, "spawn", ["wizard", 0]);

            writer.section();
            writer.writeEvent(0, "spawn", ["archer", 400]);
            writer.writeEvent(0, "spawn", ["archer", -400]);
            writer.repeatEvent({startTime: 0, duration: 2, stepTime: 1, stepY: 100}, "spawn", ["pit", -300]);
            writer.repeatEvent({startTime: 0, duration: 2, stepTime: 1, stepY: -100}, "spawn", ["pit", 300]);
            writer.repeatEvent({startTime: 1, duration: 4, stepTime: 1, stepY: 0}, "powerup", ["gold", 300]);
            writer.repeatEvent({startTime: 1.5, duration: 3, stepTime: 1, stepY: 0}, "spawn", ["wall", 300]);
            writer.repeatEvent({startTime: 1.5, duration: 3, stepTime: 1, stepY: 0}, "powerup", ["gold", 200]);
            writer.repeatEvent({startTime: 1, duration: 4, stepTime: 1, stepY: 0}, "powerup", ["gold", -300]);
            writer.repeatEvent({startTime: 1.5, duration: 3, stepTime: 1, stepY: 0}, "spawn", ["wall", -300]);
            writer.repeatEvent({startTime: 1.5, duration: 3, stepTime: 1, stepY: 0}, "powerup", ["gold", -200]);
            writer.writeEvent(2, "powerup", ["gold", 0]);
            writer.writeEvent(3, "spawn", ["pit", 100]);
            writer.writeEvent(3, "powerup", ["health", 0, 5000]);
            writer.writeEvent(3, "spawn", ["pit", -100]);
            writer.writeEvent(3, "spawn", ["pit", -400]);
            writer.writeEvent(3, "spawn", ["pit", 400]);
            writer.writeEvent(4, "powerup", ["gold", 0]);
            writer.repeatEvent({startTime: 4, duration: 2, stepTime: 1, stepY: 100}, "spawn", ["pit", 100]);
            writer.repeatEvent({startTime: 4, duration: 2, stepTime: 1, stepY: -100}, "spawn", ["pit", -100]);

            writer.section(3);
            writer.writeEvent(0, "spawn", ["wall", -200]);
            writer.writeEvent(0, "spawn", ["wall", -300]);

            writer.section(1.5);
            writer.repeatEvent({startTime: 0, duration: 6, stepTime: 0.5, stepY: 0}, "spawn", ["wall", -400]);
            writer.repeatEvent({startTime: 0, duration: 6, stepTime: 0.5, stepY: 0}, "spawn", ["wall", -100]);
            writer.repeatEvent({startTime: 0, duration: 6, stepTime: 0.5, stepY: 0}, "spawn", ["pit", 0]);
            writer.writeEvent(0, "spawn", ["wall", 400]);
            writer.writeEvent(0, "spawn", ["wall", 400]);
            writer.writeEvent(0, "powerup", ["gold", -300]);
            writer.writeEvent(0, "powerup", ["gold", -200]);
            writer.writeEvent(2, "powerup", ["gold", -300]);
            writer.writeEvent(3, "spawn", ["wizard", 400]);
            writer.writeEvent(4, "spawn", ["wizard", 200]);
            writer.writeEvent(4.5, "powerup", ["gold", 200]);
            writer.writeEvent(5, "spawn", ["wizard", 400]);
            writer.writeEvent(5.5, "powerup", ["gold", -200]);
            writer.writeEvent(6, "spawn", ["golem", -200]);
            writer.writeEvent(6, "spawn", ["golem", -300]);

            writer.section();
            writer.repeatEvent({startTime: 0, duration: 3, stepTime: 0.5, stepY: 50}, "spawn", ["wall", -400]);
            writer.repeatEvent({startTime: 0, duration: 3, stepTime: 0.5, stepY: 50}, "spawn", ["wall", -100]);
            writer.repeatEvent({startTime: 0, duration: 2, stepTime: 0.5, stepY: 50}, "spawn", ["pit", 0]);
            writer.repeatEvent({startTime: 3, duration: 3, stepTime: 0.5, stepY: 0}, "spawn", ["wall", -100]);
            writer.repeatEvent({startTime: 3, duration: 10, stepTime: 0.5, stepY: 0}, "spawn", ["wall", 200]);

            writer.writeEvent(2, "powerup", ["gold", -100]);
            writer.writeEvent(3, "powerup", ["gold", 300]);
            writer.writeEvent(3.5, "powerup", ["gold", 0]);
            writer.writeEvent(5, "spawn", ["golem", 300]);
            writer.writeEvent(6, "spawn", ["wizard", 400]);
            writer.repeatEvent({startTime: 6.5, duration: 3, stepTime: 0.5, stepY: -50}, "spawn", ["wall", -100]);
            writer.writeEvent(11, "powerup", ["gold", 100]);
            writer.writeEvent(11.5, "powerup", ["gold", 100]);
            writer.writeEvent(11.5, "powerup", ["gold", 0]);
            writer.writeEvent(12, "powerup", ["gold", 0]);
            writer.writeEvent(12, "powerup", ["gold", -100]);
            writer.writeEvent(12.5, "powerup", ["gold", -100]);
            writer.writeEvent(12, "spawn", ["wall", 100]);
            writer.writeEvent(12.5, "spawn", ["wall", 0]);
            writer.writeEvent(13, "spawn", ["wall", -100]);
            writer.repeatEvent({startTime: 13, duration: 3, stepTime: 0.5, stepY: -50}, "spawn", ["wall", 200]);
            writer.writeEvent(13, "spawn", ["archer", 100]);
            writer.writeEvent(13.5, "spawn", ["archer", 0]);
            writer.writeEvent(14, "spawn", ["archer", -100]);
            writer.writeEvent(14, "spawn", ["wizard", -300]);
            writer.repeatEvent({startTime: 16, duration: 6, stepTime: 0.5, stepY: 0}, "spawn", ["wall", -100]);
            writer.writeEvent(22, "spawn", ["wall", 0]);
            writer.writeEvent(22, "spawn", ["wall", 100]);

            writer.section(2);
            writer.writeEvent(0, "spawn", ["archer", 300, -200]);
            writer.writeEvent(0, "spawn", ["archer", -300, 200]);
            writer.writeEvent(0, "powerup", ["health", 0, 200]);
            writer.writeEvent(1, "spawn", ["archer", 200, -400]);
            writer.writeEvent(3, "spawn", ["wizard", 200]);
            writer.writeEvent(3, "spawn", ["wizard", -200]);
            writer.writeEvent(5, "spawn", ["archer", -300, 300]);
            writer.writeEvent(5, "spawn", ["archer", 300, -300]);
            writer.writeEvent(5.5, "spawn", ["archer", 100, -200]);
            writer.writeEvent(7, "spawn", ["golem caster", 300, -200]);
            writer.writeEvent(7, "spawn", ["golem caster", 100, -200]);
            writer.writeEvent(7, "spawn", ["golem caster", -100, -200]);

            writer.endLevel();

//            console.log(writer.getStats());
        }

    };
});
