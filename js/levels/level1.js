define(["game/hero", "game/writer"], function (heroHelper, writer)
{
    // wizard fires orbs at mage location
    // golem walks forward quickly and has extra health
    // eyes orbit and fire forward

    function spawnEyes(seconds, y, count)
    {
        for (var ndx = 0; ndx < count; ndx++)
        {
            writer.writeEvent(seconds, "spawn", ["eye", y, ndx / count]);
        }
    }

    function golemMarch(seconds, y)
    {
        writer.repeatEvent({startTime: seconds, duration: 1.5, stepTime: 0.5, stepY: 0}, "spawn", ["golem", y]);
        writer.writeEvent(seconds + 1, "spawn", ["wizard", y]);
    }

    return {
        name: "Wizard's Castle",
        music: "DST-ClickThenAction",
        setup: function ()
        {
            heroHelper.spawn();

            writer.newLevel({tile: "stone"});

            writer.section();
            writer.multiEvent({time: 0, count: 2, delta: 500}, "spawn", ["wizard", 0]);
            writer.multiEvent({time: 2, count: 2, delta: 500}, "powerup", ["gold", 0]);
            writer.multiEvent({time: 1, count: 5, delta: 150}, "spawn", ["golem", 0]);

            writer.section(4);
            spawnEyes(0, -250, 3);
            spawnEyes(0, 250, 3);
            writer.writeEvent(4, "powerup", ["health", 0, 50]);
            writer.writeEvent(4, "wait");

            writer.section(2);
            spawnEyes(0, 0, 4);
            writer.repeatEvent({startTime: 2, duration: 2, stepTime: 0.5, stepY: 200}, "spawn", ["wizard", -400]);
            writer.repeatEvent({startTime: 4, duration: 2, stepTime: 1, stepY: 400}, "powerup", ["gold", -400]);

            writer.section(4);
            writer.writeEvent(0, "powerup", ["gold", -100]);
            golemMarch(0, 0);
            writer.writeEvent(4, "powerup", ["gold", 300]);
            golemMarch(4, 200);
            writer.writeEvent(8, "powerup", ["gold", -300]);
            golemMarch(8, -200);
            writer.writeEvent(12, "powerup", ["gold", 100]);
            golemMarch(12, 0);

            writer.section(1);
            spawnEyes(0, 200, 3);
            spawnEyes(0, -200, 3);
            writer.repeatEvent({startTime: 2, duration: 4, stepTime: 2, stepY: 0}, "spawn", ["wizard", -400]);
            writer.repeatEvent({startTime: 3, duration: 4, stepTime: 2, stepY: 0}, "spawn", ["wizard", 400]);
            writer.writeEvent(4, "powerup", ["health", 0, 50]);

            writer.section();
            writer.writeEvent(0, "wait");

            writer.section(0.5);
            spawnEyes(0, 200, 3);
            spawnEyes(0, -200, 3);
            writer.repeatEvent({startTime: 3, duration: 4, stepTime: 2, stepY: 0}, "spawn", ["wizard", -400]);
            writer.repeatEvent({startTime: 2, duration: 4, stepTime: 2, stepY: 0}, "spawn", ["wizard", 400]);
            writer.repeatEvent({startTime: 2, duration: 4, stepTime: 1, stepY: 0}, "powerup", ["gold", 0]);

            writer.section();
            writer.writeEvent(0, "wait");
            writer.writeEvent(0.5, "powerup", ["multi", 200, 3, 20]);
            writer.writeEvent(0.5, "powerup", ["health", -200, 100]);
            golemMarch(1, 0);

            writer.section(2);
            golemMarch(3, 0);
            spawnEyes(0, 300, 5);
            spawnEyes(0, -300, 5);
            writer.multiEvent({time: 5, count: 4, delta: 200}, "powerup", ["gold", 0]);

            writer.section(8);
            golemMarch(3, 300);
            spawnEyes(0, 0, 5);
            spawnEyes(0, -300, 5);
            writer.multiEvent({time: 5, count: 5, delta: 200}, "powerup", ["gold", 0]);

            writer.section(8);
            golemMarch(3, -300);
            spawnEyes(0, 0, 5);
            spawnEyes(0, 300, 5);
            writer.multiEvent({time: 5, count: 4, delta: 200}, "powerup", ["gold", 0]);

            writer.section(4);
            writer.writeEvent(0, "powerup", ["gold", -100]);
            golemMarch(0, 0);
            writer.writeEvent(4, "powerup", ["gold", 300]);
            golemMarch(4, 200);
            writer.writeEvent(8, "powerup", ["gold", -300]);
            golemMarch(8, -200);
            writer.writeEvent(12, "powerup", ["gold", 100]);
            golemMarch(12, 0);

            writer.section(0.5);
            spawnEyes(0, 200, 3);
            spawnEyes(0, -200, 3);
            writer.repeatEvent({startTime: 3, duration: 4, stepTime: 2, stepY: 0}, "spawn", ["wizard", -400]);
            writer.repeatEvent({startTime: 2, duration: 4, stepTime: 2, stepY: 0}, "spawn", ["wizard", 400]);
            writer.repeatEvent({startTime: 2, duration: 4, stepTime: 1, stepY: 0}, "powerup", ["gold", 0]);

            writer.section(2);
            golemMarch(3, 0);
            spawnEyes(0, 300, 5);
            spawnEyes(0, -300, 5);
            writer.multiEvent({time: 5, count: 3, delta: 100}, "powerup", ["gold", 300]);
            writer.multiEvent({time: 7, count: 3, delta: 100}, "powerup", ["gold", 0]);
            writer.multiEvent({time: 9, count: 3, delta: 100}, "powerup", ["gold", -300]);

            writer.section(5);
            writer.writeEvent(5, "wait");
            writer.endLevel();

//            console.log(writer.getStats());
        }
    };
});
