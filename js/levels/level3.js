define(["game/hero", "game/writer"], function (heroHelper, writer)
{
    function spawnEyes(seconds, y, count)
    {
        for (var ndx = 0; ndx < count; ndx++)
        {
            writer.writeEvent(seconds, "spawn", ["eye", y, ndx / count]);
        }
    }

    //noinspection SpellCheckingInspection
    return {
        name: "Goblin Castle",
        music: "DST-RockitFlarex",
        setup: function ()
        {
            heroHelper.spawn();

            writer.newLevel({tile: "dark-stone"});

            writer.section();
            writer.writeEvent(0, "spawn", ["wizard", 200]);
            writer.writeEvent(0, "spawn", ["wizard", -200]);
            writer.writeEvent(4, "spawn", ["golem caster", -400]);
            writer.writeEvent(4, "spawn", ["golem caster", 400]);
            writer.writeEvent(5, "spawn", ["charger", 0]);

            writer.section();
            spawnEyes(1, 0, 3);
            spawnEyes(3, 300, 3);
            spawnEyes(3, -300, 3);
            writer.writeEvent(4, "wait");

            writer.section();

            writer.repeatEvent({startTime: 4, duration: 1, stepTime: 0.5, stepY: 0}, "spawn", ["golem caster", -400]);
            writer.repeatEvent({startTime: 4, duration: 1, stepTime: 0.5, stepY: 0}, "spawn", ["golem caster", 400]);
            writer.repeatEvent({startTime: 1, duration: 1.5, stepTime: 1, stepY: 0}, "powerup", ["gold", 0]);
            writer.writeEvent(6, "wait");

            writer.section(1);
            writer.writeEvent(0, "spawn", ["wall", 100]);
            writer.writeEvent(0, "spawn", ["wall", -100]);
            writer.writeEvent(0.5, "spawn", ["wall", 0]);
            writer.writeEvent(0, "powerup", ["speed", 0, 2, 20]);
            writer.writeEvent(0.5, "spawn", ["archer", 100]);
            writer.writeEvent(0.5, "spawn", ["archer", -100]);
            writer.writeEvent(1, "spawn", ["archer", 0]);

            writer.section(2);
            spawnEyes(3, 300, 5);
            spawnEyes(4, 0, 5);
            spawnEyes(3, -300, 5);
            writer.writeEvent(5, "powerup", ["gold", -400]);
            writer.writeEvent(6, "powerup", ["gold", 0]);
            writer.writeEvent(5, "powerup", ["gold", 400]);
            writer.writeEvent(6, "wait");

            writer.section(1);
            writer.repeatEvent({startTime: 2, duration: 3, stepTime: 1, stepY: 50}, "spawn", ["flying bomb", -400]);
            writer.repeatEvent({startTime: 2, duration: 3, stepTime: 1, stepY: -50}, "spawn", ["flying bomb", 400]);
            writer.writeEvent(2, "spawn", ["wizard", 400, -200]);
            writer.writeEvent(2, "spawn", ["wizard", -400, 200]);
            writer.writeEvent(2, "spawn", ["golem caster", 100, -300]);

            writer.writeEvent(4, "spawn", ["golem caster", 300, -300]);
            writer.writeEvent(4, "spawn", ["golem caster", -300, 300]);
            writer.writeEvent(4, "spawn", ["archer", 200, -100]);
            writer.writeEvent(4, "spawn", ["archer", 200, -100]);
            writer.writeEvent(5, "spawn", ["golem", 0]);
            writer.writeEvent(5.5, "spawn", ["golem", 0]);
            writer.writeEvent(6, "spawn", ["golem", 0]);

            writer.section(1);
            writer.writeEvent(0, "powerup", ["health", 0, 200]);
            spawnEyes(5, 0, 7);
            writer.writeEvent(6, "spawn", ["wizard", 400]);
            writer.writeEvent(6.5, "powerup", ["gold", 400]);
            writer.writeEvent(6, "spawn", ["wizard", -400]);
            writer.writeEvent(6.5, "powerup", ["gold", -400]);
            writer.writeEvent(7.5, "spawn", ["wizard", 400]);
            writer.writeEvent(8, "powerup", ["gold", 400]);
            writer.writeEvent(7.5, "spawn", ["wizard", -400]);
            writer.writeEvent(8, "powerup", ["gold", -400]);
            writer.writeEvent(9, "spawn", ["wizard", 400]);
            writer.writeEvent(9.5, "powerup", ["gold", 400]);
            writer.writeEvent(9, "spawn", ["wizard", -400]);
            writer.writeEvent(9.5, "powerup", ["gold", -400]);

            writer.section(1);
            writer.repeatEvent({startTime: 2, duration: 6, stepTime: 2, stepY: 0}, "powerup", ["gold", 100]);
            writer.repeatEvent({startTime: 3, duration: 6, stepTime: 2, stepY: 0}, "powerup", ["gold", -100]);
            writer.repeatEvent({startTime: 5, duration: 6, stepTime: 2, stepY: 50}, "spawn", ["archer", -300]);
            writer.repeatEvent({startTime: 5, duration: 6, stepTime: 2, stepY: -50}, "spawn", ["archer", 300]);
            writer.repeatEvent({startTime: 6, duration: 6, stepTime: 2, stepY: 50}, "spawn",
                ["golem caster", 100, -200]);

            writer.section(1);
            writer.writeEvent(1, "powerup", ["gold", -400]);
            writer.writeEvent(1.5, "powerup", ["multi", -400, 3, 15]);
            writer.writeEvent(2, "powerup", ["gold", -400]);
            writer.writeEvent(1, "powerup", ["gold", 400]);
            writer.writeEvent(1.5, "powerup", ["health", 400, 100]);
            writer.writeEvent(2, "powerup", ["gold", 400]);

            writer.section(1);
            spawnEyes(4, 300, 5);
            spawnEyes(4, -300, 5);
            spawnEyes(9, -200, 5);
            spawnEyes(9, 200, 5);
            writer.writeEvent(10, "wait");

            writer.section();
            spawnEyes(1, 0, 10);
            writer.repeatEvent({startTime: 3, duration: 6, stepTime: 1, stepY: 0}, "powerup", ["gold", 0]);
            writer.repeatEvent({startTime: 5, duration: 6, stepTime: 2, stepY: 0}, "spawn", ["golem caster", 100]);
            writer.repeatEvent({startTime: 5, duration: 6, stepTime: 2, stepY: 0}, "spawn", ["golem caster", -100]);
            writer.writeEvent(12, "wait");

            writer.section();
            writer.writeEvent(1, "spawn", ["wall", 100]);
            writer.writeEvent(1, "spawn", ["wall", -100]);
            writer.writeEvent(1.5, "spawn", ["wizard", 100]);
            writer.writeEvent(1.5, "spawn", ["wizard", -100]);
            writer.repeatEvent({startTime: 2, duration: 6, stepTime: 2, stepY: 0}, "spawn", ["golem", 0]);
            writer.writeEvent(2, "spawn", ["wall", 200]);
            writer.writeEvent(2, "spawn", ["wall", -200]);
            writer.writeEvent(2.5, "spawn", ["wizard", 200]);
            writer.writeEvent(2.5, "spawn", ["wizard", -200]);
            writer.writeEvent(3, "spawn", ["pit", -300]);
            writer.writeEvent(3.5, "spawn", ["pit", -400]);
            writer.writeEvent(3, "spawn", ["pit", 300]);
            writer.writeEvent(3.5, "spawn", ["pit", 400]);
            writer.writeEvent(3.5, "powerup", ["gold", 200]);
            writer.writeEvent(3.5, "powerup", ["gold", -200]);
            writer.writeEvent(4, "powerup", ["gold", 100]);
            writer.writeEvent(4, "powerup", ["gold", -100]);
            writer.writeEvent(4, "powerup", ["gold", 400]);
            writer.writeEvent(4, "powerup", ["gold", -400]);
            writer.writeEvent(4.5, "powerup", ["gold", 0]);

            writer.section();
            writer.writeEvent(1, "spawn", ["wall", 400]);
            writer.writeEvent(1, "spawn", ["wall", -400]);
            writer.writeEvent(1.5, "spawn", ["archer", 400]);
            writer.writeEvent(1.5, "spawn", ["archer", -400]);
            writer.writeEvent(1, "spawn", ["wall", 300]);
            writer.writeEvent(1, "spawn", ["wall", -300]);
            writer.writeEvent(1.5, "spawn", ["archer", 300]);
            writer.writeEvent(1.5, "spawn", ["archer", -300]);
            writer.writeEvent(2, "powerup", ["gold", 400]);
            writer.writeEvent(2, "powerup", ["gold", -400]);
            writer.writeEvent(3, "spawn", ["charger", 200, -100]);
            writer.writeEvent(3, "spawn", ["charger", -200, 100]);

            writer.endLevel();

//            console.log(writer.getStats());

        }

    };
});
