define(["game/hero", "game/writer"], function (heroHelper, writer)
{
    function tieFighters(seconds, y)
    {
        writer.writeEvent(seconds, "spawn", ["charger", y + 100, -200]);
        writer.multiEvent({time: seconds + 0.5, count: 2, delta: 200}, "spawn", ["archer", y]);
    }

    return {
        name: "Goblin Fields",
        music: "DST-MirrorMix",
        setup: function ()
        {
            heroHelper.spawn();

            writer.newLevel({tile: "grass"});
/* */
            writer.section();
            writer.repeatEvent({startTime: 0, duration: 1, stepTime: 0.5, stepY: -120}, "spawn", ["pit", 400]);
            writer.repeatEvent({startTime: 2, duration: 1, stepTime: 0.5, stepY: 120}, "spawn", ["pit", -200]);
            writer.repeatEvent({startTime: 5, duration: 1, stepTime: 0.5, stepY: 120}, "spawn", ["pit", -400]);
            writer.repeatEvent({startTime: 5, duration: 1, stepTime: 0.5, stepY: -120}, "spawn", ["pit", 400]);

            writer.section(3.5);
            writer.multiEvent({time: 0, count: 3, delta: 250}, "spawn", ["pit", 0]);
            writer.multiEvent({time: 0, count: 2, delta: 250}, "powerup", ["gold", 0]);
            writer.multiEvent({time: 0.5, count: 3, delta: 200}, "spawn", ["archer", -200, 200]);
            writer.multiEvent({time: 2.5, count: 3, delta: 200}, "spawn", ["archer", 200, -200]);

            writer.section(2.5);
            writer.multiEvent({time: 0, count: 3, delta: 250}, "spawn", ["pit", 150]);
            writer.multiEvent({time: 0, count: 2, delta: 250}, "powerup", ["gold", 150]);
            writer.multiEvent({time: 0.5, count: 3, delta: 200}, "spawn", ["archer", 0, 200]);
            writer.multiEvent({time: 2.5, count: 3, delta: 200}, "spawn", ["archer", 0, -200]);

            writer.section(2.5);
            writer.multiEvent({time: 0, count: 3, delta: 250}, "spawn", ["pit", -150]);
            writer.multiEvent({time: 0, count: 2, delta: 250}, "powerup", ["gold", -150]);
            writer.multiEvent({time: 0.5, count: 3, delta: 200}, "spawn", ["archer", -200, 200]);
            writer.multiEvent({time: 2.5, count: 3, delta: 200}, "spawn", ["archer", 200, -200]);
            writer.writeEvent(2.5, "wait");

            writer.writeEvent(3, "powerup", ["health", 0, 100]);

            writer.section(0.5);
            writer.repeatEvent({startTime: 0, duration: 8, stepTime: 1, stepY: 100}, "spawn", ["archer", -400]);
            writer.repeatEvent({startTime: 2, duration: 8, stepTime: 1, stepY: 100}, "spawn", ["charger", -400]);
            writer.repeatEvent({startTime: 12, duration: 8, stepTime: 1, stepY: -100}, "spawn", ["archer", 400]);
            writer.repeatEvent({startTime: 14, duration: 8, stepTime: 1, stepY: -100}, "spawn", ["charger", 400]);

            writer.writeEvent(1, "powerup", ["gold", -400]);
            writer.writeEvent(11, "powerup", ["gold", -400]);
            writer.writeEvent(4, "powerup", ["gold", -100]);
            writer.writeEvent(16, "powerup", ["gold", -100]);
            writer.writeEvent(9, "powerup", ["gold", 400]);
            writer.writeEvent(11, "powerup", ["gold", 400]);
            writer.writeEvent(13, "powerup", ["gold", 400]);
            writer.writeEvent(8, "powerup", ["gold", 300]);
            writer.writeEvent(10, "powerup", ["gold", 300]);
            writer.writeEvent(12, "powerup", ["gold", 300]);
            writer.writeEvent(14, "powerup", ["gold", 300]);

            writer.section();
            writer.writeEvent(0.5, "powerup", ["gold", 350]);
            writer.multiEvent({time: 1, count: 4, delta: 100}, "spawn", ["pit", 250]);
            writer.multiEvent({time: 3, count: 3, delta: 200}, "spawn", ["charger", -200, 400]);

            writer.section(1);
            tieFighters(0, 0);
            tieFighters(2, -200);
            tieFighters(4, 200);
            tieFighters(6, 0);

            writer.section();
            writer.writeEvent(0.5, "powerup", ["gold", -350]);
            writer.multiEvent({time: 1, count: 4, delta: 100}, "spawn", ["pit", -250]);
            writer.multiEvent({time: 2, count: 3, delta: 200}, "spawn", ["charger", 200, -400]);

            writer.section(1);
            writer.repeatEvent({startTime: 0, duration: 1, stepTime: 0.5, stepY: -120}, "spawn", ["pit", 400]);
            writer.repeatEvent({startTime: 2, duration: 1, stepTime: 0.5, stepY: 120}, "spawn", ["pit", -200]);
            writer.repeatEvent({startTime: 5, duration: 1, stepTime: 0.5, stepY: 120}, "spawn", ["pit", -400]);
            writer.repeatEvent({startTime: 5, duration: 1, stepTime: 0.5, stepY: -120}, "spawn", ["pit", 400]);

            writer.section(1.5);
            writer.multiEvent({time: 0, count: 2, delta: 150}, "spawn", ["archer", 0]);
            writer.multiEvent({time: 1, count: 3, delta: 150}, "spawn", ["archer", 0]);
            writer.multiEvent({time: 2, count: 4, delta: 150}, "spawn", ["archer", 0]);
            writer.multiEvent({time: 3, count: 5, delta: 150}, "spawn", ["archer", 0]);
            writer.multiEvent({time: 4, count: 3, delta: 200}, "powerup", ["gold", 0]);
            writer.multiEvent({time: 4.5, count: 3, delta: 200}, "spawn", ["pit", 0]);

            writer.section(2.5);
            writer.multiEvent({time: 0, count: 3, delta: 250}, "spawn", ["pit", 150]);
            writer.multiEvent({time: 0, count: 2, delta: 250}, "powerup", ["gold", 150]);
            writer.multiEvent({time: 0.5, count: 3, delta: 200}, "spawn", ["archer", 0, 200]);
            writer.multiEvent({time: 2.5, count: 3, delta: 200}, "spawn", ["archer", 0, -200]);

            writer.section(2.5);
            writer.multiEvent({time: 0, count: 3, delta: 250}, "spawn", ["pit", -150]);
            writer.multiEvent({time: 0, count: 2, delta: 250}, "powerup", ["gold", -150]);
            writer.multiEvent({time: 0.5, count: 3, delta: 200}, "spawn", ["archer", -200, 200]);
            writer.multiEvent({time: 2.5, count: 3, delta: 200}, "spawn", ["archer", 200, -200]);
            writer.writeEvent(2.5, "wait");

            writer.section(1);
            writer.multiEvent({time: 0, count: 3, delta: 200}, "spawn", ["charger", 200, -400]);
            writer.multiEvent({time: 2, count: 3, delta: 200}, "spawn", ["charger", 200, -400]);

            writer.section(0.5);
            writer.writeEvent(0, "powerup", ["speed", 300, 2, 20]);
            writer.writeEvent(0, "powerup", ["health", 0, 50]);
            writer.writeEvent(0, "powerup", ["multi", -300, 3, 20]);
            tieFighters(1, 300);
            tieFighters(1, -300);

            writer.section(3);
            writer.multiEvent({time: 0, count: 3, delta: 200}, "spawn", ["charger", 200, -400]);
            writer.multiEvent({time: 0.5, count: 5, delta: 150}, "spawn", ["archer", 100]);
            writer.multiEvent({time: 3, count: 3, delta: 200}, "spawn", ["charger", 200, -400]);
            writer.multiEvent({time: 3.5, count: 5, delta: 150}, "spawn", ["archer", -100]);

            writer.section(2);
            writer.repeatEvent({startTime: 0, duration: 1, stepTime: 0.5, stepY: -120}, "spawn", ["pit", 200]);
            writer.repeatEvent({startTime: 0, duration: 1, stepTime: 0.5, stepY: -120}, "spawn", ["pit", -100]);
            writer.multiEvent({time: 2, count: 5, delta: 150}, "spawn", ["archer", 100]);
            writer.multiEvent({time: 3, count: 2, delta: 200}, "powerup", ["gold", -200]);

            writer.section(2);
            tieFighters(0, -300);
            tieFighters(0, 100);
            tieFighters(2, -200);
            tieFighters(2, 200);
            tieFighters(4, -100);
            tieFighters(4, 300);

            writer.section(2);
            writer.repeatEvent({startTime: 0, duration: 1, stepTime: 0.5, stepY: 120}, "spawn", ["pit", -200]);
            writer.repeatEvent({startTime: 0, duration: 1, stepTime: 0.5, stepY: 120}, "spawn", ["pit", 100]);
            writer.multiEvent({time: 2, count: 5, delta: 150}, "spawn", ["archer", -100]);
            writer.multiEvent({time: 3, count: 2, delta: 200}, "powerup", ["gold", 0]);

            writer.section(2);
            writer.repeatEvent({startTime: 0, duration: 1, stepTime: 0.5, stepY: 120}, "spawn", ["pit", -400]);
            writer.repeatEvent({startTime: 0, duration: 1, stepTime: 0.5, stepY: 120}, "spawn", ["pit", -100]);
            writer.multiEvent({time: 2, count: 5, delta: 150}, "spawn", ["archer", 0]);
            writer.multiEvent({time: 3, count: 2, delta: 200}, "powerup", ["gold", 200]);

            writer.section(2);
            tieFighters(0, 300);
            tieFighters(0, -100);
            tieFighters(2, 200);
            tieFighters(2, -200);
            tieFighters(4, 100);
            tieFighters(4, -300);

            writer.endLevel();

            //console.log(writer.getStats());
        }
    };
});
