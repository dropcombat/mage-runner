define("entity", function ()
{
    var nextEntityId = 1;
    // map keyed off an id
    // value is humanReadableName, for debugging
    var tableEntities = {};

    // map keyed off of name
    // values have the following properties:
    // description - human readable description of the component
    // tableName - name of the table used to find in tableComponentDataContainers
    var tableComponents = {};

    // the following two maps contain the same data
    var tableEntityComponentsKeyEntity = {};
    var tableEntityComponentsKeyComponent = {};

    // count of entities using a component
    var tableEntityComponentsCount = {};

    // map keyed off tableName (from tableComponents)
    // values have the following properties:
    // nextId - the next id to use when adding to the table
    // table - the table
    // createRecord - function returning a new record
    var tableComponentDataContainers = {};

    var systems = {};

    var tableTempEntityComponentAdded = {};
    var tableTempEntityComponentRemoved = {};

    // TODO: Define functions in this scope so we can avoid extra lookup.
    //noinspection JSUnusedGlobalSymbols,JSUnusedGlobalSymbols,JSUnusedGlobalSymbols
    var functions = {
        // Creates entity, returns entityId, linked in a table with a human readable name. Also creates all required subtables.
        createEntity: function (humanReadableName)
        {
            //noinspection IncrementDecrementResultUsedJS
            var entityId = nextEntityId++;
            // humanReadableName is optional, should be for debugging only.
            tableEntities[entityId] = humanReadableName;
            tableEntityComponentsKeyEntity[entityId] = {};
            tableTempEntityComponentAdded[entityId] = {};
            tableTempEntityComponentRemoved[entityId] = {};
            return entityId;
        },
        // Removes entity from entity table, and removes all associated components.
        deleteEntity: function (entityId, skipYield)
        {
            var execute = function ()
            {
                functions.removeAllComponentsFromEntity(entityId);
                functions.refreshEntity(entityId);
                delete tableEntities[entityId];
            };
            // yield delete so it doesn't execute while a system is updating if needed
            //noinspection DynamicallyGeneratedCodeJS
            skipYield ? execute() : setTimeout(execute, 0);
        },
        // Deletes all entities and removes components.
        deleteAllEntities: function ()
        {
            for (var id in tableEntities)
            {
                // skip yield
                functions.deleteEntity(id, 1);
            }
        },
        // checks to see if an entity exists
        hasEntity: function (entityId)
        {
            return typeof tableEntities[entityId] != "undefined";
        },
        // Refreshes the entity, checks to see if components are added or removed from a system. and more stuff too but thomas is too drunk to explain, lol.
        refreshEntity: function (entityId)
        {
            // add / remove systems based on component changes
            for (var systemName in systems)
            {
                var system = systems[systemName];
                var componentsInSystem = system.componentNames;
                var componentsAddedToSystem = tableTempEntityComponentAdded[entityId];
                var componentsRemovedFromSystem = tableTempEntityComponentRemoved[entityId];
                var shouldBeInSystem = true;
                var wasInSystem = true;
                var componentName;
                for (var ndx = 0; ndx < componentsInSystem.length; ndx++)
                {
                    componentName = componentsInSystem[ndx];
                    //noinspection NegatedIfStatementJS
                    if (!tableEntityComponentsKeyEntity[entityId][componentName])
                    {
                        // doesn't have component
                        shouldBeInSystem = false;
                        if (!componentsRemovedFromSystem[componentName])
                        {
                            wasInSystem = false;
                        }
                    }
                    else if (componentsRemovedFromSystem[componentName])
                    {
                        shouldBeInSystem = false;
                    }
                    else if (componentsAddedToSystem[componentName])
                    {
                        wasInSystem = false;
                    }
                    // should never have both componentsRemovedFromSystem and componentsAddedToSystem at the same time
                    // there are no checks for that right now though...
                }
                if (wasInSystem && !shouldBeInSystem)
                {
                    system.removeEntity && system.removeEntity(entityId);
                    for (var ndx2 = 0; ndx2 < system.entityList.length; ndx2++)
                    {
                        if (system.entityList[ndx2] == entityId)
                        {
                            system.entityList.splice(ndx2, 1);
                            //noinspection BreakStatementJS
                            break;
                        }
                    }
                }
                else if (!wasInSystem && shouldBeInSystem)
                {
                    system.addEntity && system.addEntity(entityId);
                    system.entityList.push(entityId);
                }
            }

            // clear added table
            for (componentName in tableTempEntityComponentRemoved[entityId])
            {
                var dataId = tableEntityComponentsKeyEntity[entityId][componentName];
                delete tableComponentDataContainers[tableComponents[componentName].tableName].table[dataId];
                delete tableEntityComponentsKeyEntity[entityId][componentName];
                delete tableEntityComponentsKeyComponent[componentName][entityId];
                tableEntityComponentsCount[componentName]--;
            }
            tableTempEntityComponentAdded[entityId] = {};
            tableTempEntityComponentRemoved[entityId] = {};
        },
        // Creates a component.
        // name: Name of the component used in the code.
        // description: Human readable description of the component.
        // tableName: Name to store data under.
        // dataCreationFunction: Creates default data for component.
        // name and tableName are separate to allow for changing the name without needing to refactor.
        createComponent: function (name, description, tableName, dataCreationFunction)
        {
            //noinspection JSUnusedGlobalSymbols
            tableComponents[name] = {
                description: description,
                tableName: tableName
            };
            tableComponentDataContainers[tableName] = {
                nextId: 1,
                table: {},
                createRecord: dataCreationFunction
            };
            tableEntityComponentsKeyComponent[name] = {};
            tableEntityComponentsCount[name] = 0;
            return name;
        },
        // Adds component to entity, must call refreshEntity after.
        addComponentToEntity: function (componentName, entityId)
        {
            var componentDataContainer = tableComponentDataContainers[tableComponents[componentName].tableName];
            //noinspection IncrementDecrementResultUsedJS
            var dataId = componentDataContainer.nextId++;
            componentDataContainer.table[dataId] = componentDataContainer.createRecord();
            tableEntityComponentsKeyEntity[entityId][componentName] = dataId;
            tableEntityComponentsKeyComponent[componentName][entityId] = dataId;
            tableEntityComponentsCount[componentName]++;

            tableTempEntityComponentAdded[entityId][componentName] = true;
        },
        // Removes all components from entity. Used mostly by deleteEntity
        removeAllComponentsFromEntity: function (entityId)
        {
            for (var componentName in tableEntityComponentsKeyEntity[entityId])
            {
                functions.removeComponentFromEntity(componentName, entityId);
            }
        },
        // Removes component from entity. refreshEntity must be called after.
        removeComponentFromEntity: function (componentName, entityId)
        {
            // not removed until refreshEntity is called
            tableTempEntityComponentRemoved[entityId][componentName] = true;
        },
        // Gets the component data for an entity, gets key by component.
        getComponentDataForEntity: function (componentName, entityId)
        {
            return tableComponentDataContainers[tableComponents[componentName].tableName].table[tableEntityComponentsKeyComponent[componentName][entityId]];
        },
        // Gets all entities that have the component name.
        getEntityByComponent: function (componentName)
        {
            return tableEntityComponentsKeyComponent[componentName];
        },
        getEntityCountByComponent: function (componentName)
        {
            return tableEntityComponentsCount[componentName];
        },
        // Creates a system.
        // update function retrieves all of the components and the entityId and timer object. called once per entity in the system.
        // systemName: The name of the system.
        // componentNames: A list of component names.
        // updateFunction: Called on every update call from gameLoop
        //      gets list of components as well as timer and entityId
        //      (component1, component2, ..., componentN, timer, entityId)
        // addFunction: This function is called whenever an entity matches the component names list.
        //      gets entityId
        // removeFunction: This function is called whenever an entity stops matching the component names list.
        //      gets entityId
        // msDeltaMin: Minimum delta in milliseconds.
        createSystem: function (systemName, componentNames, updateFunction, addFunction, removeFunction, msDeltaMin)
        {
            var length;
            var ndx;
            var ndx2;
            var componentCount = componentNames.length;
            var args = new Array(componentNames.length + 2);
            functions.createComplexSystem(systemName, componentNames, function (timer, entityList)
            {
                length = entityList.length;
                // copy list over in case entities get removed while the system is updating
                var tempList = new Array(length);
                for (ndx = 0; ndx < length; ndx++)
                {
                    tempList[ndx] = entityList[ndx];
                }
                for (ndx = 0; ndx < length; ndx++)
                {
                    for (ndx2 = 0; ndx2 < componentCount; ndx2++)
                    {
                        args[ndx2] = functions.getComponentDataForEntity(componentNames[ndx2], tempList[ndx]);
                    }
                    args[ndx2] = timer;
                    ndx2++;
                    args[ndx2] = tempList[ndx];
                    updateFunction.apply(null, args);
                }
            }, addFunction, removeFunction, msDeltaMin);
        },
        // Creates a system with the following inputs.
        // update function receives a timer object and a list of entityIds
        // systemName: The name of the system.
        // componentNames: The name of the components included in the system.
        // updateFunction: Called on every update call from gameLoop
        //      timer and entityList (array of entityIds)
        // addFunction: This function is called whenever an entity matches the component names list.
        //      gets entityId
        // removeFunction: This function is called whenever an entity stops matching the component names list.
        //      gets entityId
        // msDeltaMin: The minimum time past before calling update.
        // TODO: Actually fucking do something with msDeltaMin.
        createComplexSystem: function (systemName, componentNames, updateFunction, addFunction, removeFunction,
                                       msDeltaMin)
        {
            var nextUpdate = +new Date;
            var deltaUpdate = (msDeltaMin) ? function ()
            {
                var time = +new Date;
                if (time >= nextUpdate)
                {
                    nextUpdate = time + msDeltaMin;
                    updateFunction.apply(null, arguments);
                }
            } : updateFunction;
            systems[systemName] = {
                componentNames: componentNames,
                update: deltaUpdate,
                addEntity: addFunction,
                removeEntity: removeFunction,
                entityList: []
            };
            // call addFunction for all matching entities if we want to support adding systems during runtime
        },
        // gets a system by name
        // systemName: the name of the system
        getSystem: function (systemName)
        {
            return systems[systemName];
        },
        // updates a system with given timeInfo
        // systemName: the name of the system
        // timeInfo: object with time information
        updateSystem: function (systemName, timeInfo)
        {
            var system = systems[systemName];
            system && system.update && system.update(timeInfo, system.entityList);
        }
    };

    /* useful for debugging issues * /
     window.debug = {
     tableEntities : tableEntities,
     tableComponents : tableComponents,
     tableEntityComponentsKeyEntity : tableEntityComponentsKeyEntity,
     tableEntityComponentsKeyComponent : tableEntityComponentsKeyComponent,
     tableComponentDataContainers : tableComponentDataContainers,
     systems : systems,
     tableTempEntityComponentAdded : tableTempEntityComponentAdded,
     tableTempEntityComponentRemoved : tableTempEntityComponentRemoved
     };
     /* end debug */
    return functions;
});
