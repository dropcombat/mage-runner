define(["entity", "core/message"], function (entity, message)
{

    var updateSystems = [];
    var pausedSystems = [];
    var renderSystems = [];
    var running = false;
    var paused = false;
    var lastTime = 0;
    var timeInfo = {
        time: 0,
        delta: 0
    };
    //noinspection SpellCheckingInspection
    var requestAnimFrame = (function ()
    {
        //noinspection OverlyComplexBooleanExpressionJS
        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame
                   || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function (callback)
               {
                   //noinspection DynamicallyGeneratedCodeJS
                   window.setTimeout(callback, 16.7);
               };
    })();

    // use this funciton to test a long skip in time
    // tested at 50000000 (just over 13 hours)
    // and it takes a while to recover, but it does eventually
//    window.skipTime = function(ms)
//    {
//        lastTime -= ms;
//    };

    //noinspection JSUnusedGlobalSymbols
    return {
        start: function ()
        {
            paused = false;
            if (!running)
            {
                running = true;
                lastTime = +new Date;
                requestAnimFrame(loop);
            }
        },
        stop: function ()
        {
            running = false;
        },
        setPausedSystems: function (pausedSys)
        {
            pausedSystems = pausedSys;
        },
        setUpdateSystems: function (updateSys)
        {
            updateSystems = updateSys;
        },
        setRenderSystems: function (renderSys)
        {
            renderSystems = renderSys;

        },
        pause: function ()
        {
            paused = true;
        },
        getPause: function ()
        {
            return paused;
        }
    };
    function loop()
    {
        if (running)
        {
            var ndx;
            var time = +new Date;
            // max update time will restrict the running time for the update loop
            // so that the browser doesn't think the script is not responsive
            var maxUpdate = time + 30;
            var delta = time - lastTime;

            // update at most 100ms at a time, then loop
            while (delta > 0 && +new Date <= maxUpdate)
            {
                var localDelta = delta <= 100 ? delta : 100;
                delta -= localDelta;
                timeInfo.time += localDelta;
                timeInfo.delta = localDelta;

                var list = paused ? pausedSystems : updateSystems;
                // update all systems in correct order
                for (ndx = 0; ndx < list.length; ndx++)
                {
                    entity.updateSystem(list[ndx], timeInfo);
                    message.run();
                }
            }

            if (delta > 0)
            {
                // maxUpdate time reached, leave the rest of the update work for the next loop
                time -= delta;
            }

            timeInfo.delta = time - lastTime;
            // render systems
            for (ndx = 0; ndx < renderSystems.length; ndx++)
            {
                entity.updateSystem(renderSystems[ndx], timeInfo);
                message.run();
            }

            lastTime = time;
            requestAnimFrame(loop);
        }
    }

});
