define(function ()
{
    var imageNames = [];
    var musicNames = [];
    var sfxNames = [];
    var queue = [];
    var filters = {};

    function queueImage(src)
    {
        queue.push(function(done)
        {
            var img = new Image();

            img.onload = function ()
            {
                img.onload = null;
                done();
            };
            img.src = src;
        });
    }

    function queueAudio(src)
    {
        queue.push(function(done)
        {
            var audio = new Audio();
            //noinspection SpellCheckingInspection
            audio.addEventListener('canplaythrough', done, false);
            audio.src = src;
        });
    }

    function filterToQueue()
    {
        var ndx;

        if (imageNames.length)
        {
            if (filters.image)
            {
                for (ndx = 0; ndx < imageNames.length; ndx++)
                {
                    queueImage(filters.image(imageNames[ndx]));
                }
                imageNames = [];
            }
            else
            {
                console.log("preload: need to add image filter");
            }
        }

        if (musicNames.length)
        {
            if (filters.music)
            {
                for (ndx = 0; ndx < musicNames.length; ndx++)
                {
                    queueAudio(filters.music(musicNames[ndx]));
                }
                musicNames = [];
            }
            else
            {
                console.log("preload: need to add music filter");
            }
        }

        if (sfxNames.length)
        {
            if (filters.sfx)
            {
                for (ndx = 0; ndx < sfxNames.length; ndx++)
                {
                    queueAudio(filters.sfx(sfxNames[ndx]));
                }
                sfxNames = [];
            }
            else
            {
                console.log("preload: need to add sfx filter");
            }
        }
    }

    return {
        image: function(nameOrArray)
        {
            if (typeof nameOrArray == "string")
            {
                imageNames.push(nameOrArray);
            }
            else if (nameOrArray.splice)
            {
                imageNames = imageNames.concat(nameOrArray);
            }
        },
        music: function(nameOrArray)
        {
            if (typeof nameOrArray == "string")
            {
                musicNames.push(nameOrArray);
            }
            else if (nameOrArray.splice)
            {
                musicNames = musicNames.concat(nameOrArray);
            }
        },
        sfx: function(nameOrArray)
        {
            if (typeof nameOrArray == "string")
            {
                sfxNames.push(nameOrArray);
            }
            else if (nameOrArray.splice)
            {
                sfxNames = sfxNames.concat(nameOrArray);
            }
        },
        addFilter: function(name, filter)
        {
            if (typeof filter == "function")
            {
                filters[name] = filter;
            }
        },
        run: function (maxConcurrent, doneCallback, progressCallback)
        {
            filterToQueue();

            var startQueueLength = queue.length;
            var finished = 0;

            for (var connections = 0; connections <= maxConcurrent; connections++)
            {
                dequeue();
            }

            function dequeue()
            {
                if (!queue.length)
                {
                    return;
                }

                queue.splice(0, 1)[0](preloadCallback);
            }

            function preloadCallback()
            {
                finished++;
                //noinspection JSUnusedGlobalSymbols
                progressCallback && progressCallback({finished: finished, total: startQueueLength});
                if (queue.length)
                {
                    dequeue();
                }
                else
                {
                    doneCallback();
                }
            }
        }
    };

});
