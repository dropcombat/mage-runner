define(["entity"], function (entity)
{
    entity.createComponent("position", "x,y position", "position", function ()
    {
        return {
            x: 0,
            y: 0
        };
    });
});
