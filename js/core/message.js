define(function ()
{
    var listenerByType = {};
    var queue = [];

    return {
        queue: function (msg)
        {
//            console.log(msg.type + " " + msg.id + " " + msg.triggering);
            queue.push(msg);
        },
        run: function ()
        {
            for(var queueIndex = 0; queueIndex < queue.length; queueIndex++)
            {
                var msg = queue[queueIndex];
                var listeners = listenerByType[msg.type] || [];
                for (var listenerIndex = 0; listenerIndex < listeners.length; listenerIndex++)
                {
                    listeners[listenerIndex](msg);
                }
            }

            queue = [];
        },
        addListener: function (type, listener)
        {
            if (typeof listener != "function")
            {
                // listener must be a function
                return;
            }

            if (listenerByType[type])
            {
                listenerByType[type].push(listener);
            }
            else
            {
                listenerByType[type] = [listener];
            }
        }
    };

});
