define(function ()
{
    // try to use localStorage
    if (localStorage)
    {
        return {
            get: function (name)
            {
                return localStorage.getItem(name);
            },
            set: function (name, value)
            {
                localStorage.setItem(name, value);
            }
        };
    }

    // fallback to cookies
    var cookiesArray = document.cookie.split(";");
    var cookies = {};
    for (var ndx = 0; ndx < cookiesArray.length; ndx++)
    {
        var split = cookiesArray[ndx].split("=");
        var name = split[0].trim();
        cookies[name] = {
            name: name,
            value: split[1],
            index: ndx
        };
    }

    return {
        get: function (name)
        {
            var cookie = cookies[name];
            return cookie ? cookie.value : "";
        },
        set: function (name, value)
        {
            var cookie = cookies[name];
            if (cookie)
            {
                cookie.value = value;
            }
            else
            {
                cookie = {
                    name: name,
                    value: value,
                    index: cookiesArray.length
                };
                cookies[name] = cookie;
            }
            document.cookie = [cookie.name, cookie.value].join("=");
        }
    };
});