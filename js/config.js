// stop IE from crashing, remove before production
if (!window.console)
{
    //noinspection JSUnusedGlobalSymbols
    window.console = {
        log: function ()
        {
        }
    };
}

//noinspection SpellCheckingInspection
require.config({
    paths: {
        "domReady": "../lib/domReady",
        "entity": "core/entity",
        "Kinetic": "../lib/kinetic-v5.1.0.min"
    },
    shim: {
        "Kinetic": {
            exports: "Kinetic"
        }
    },
    urlArgs: "bust=" + +new Date
});

require(["game"]);