define(["entity"], function (entity)
{
    entity.createSystem("restrictedPosition", ["position", "restrictedPosition"], function (position, restrictedPos)
    {
        if (position.x < restrictedPos.minX)
        {
            position.x = restrictedPos.minX;
        }
        else if (position.x > restrictedPos.maxX)
        {
            position.x = restrictedPos.maxX;
        }
        if (position.y < restrictedPos.minY)
        {
            position.y = restrictedPos.minY;
        }
        else if (position.y > restrictedPos.maxY)
        {
            position.y = restrictedPos.maxY;
        }
    });
});
