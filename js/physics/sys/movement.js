define(["entity"], function (entity)
{
    // Movement just based on velocity
    entity.createSystem("movement", ["position", "velocity"], function(position, velocity, timer)
    {
        var seconds = timer.delta / 1000;
        position.x += velocity.x * seconds;
        position.y += velocity.y * seconds;
    });
});
