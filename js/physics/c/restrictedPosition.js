define(["entity"], function (entity)
{
    entity.createComponent("restrictedPosition", "min/max x,y", "restrictedPosition", function ()
    {
        return {
            minX: 0,
            maxX: 0,
            minY: 0,
            maxY: 0
        };
    });
});
