define(["entity"], function (entity)
{
    entity.createComponent("velocity", "x,y velocity", "velocity", function ()
    {
        return {
            x: 0,
            y: 0
        };
    });
});
