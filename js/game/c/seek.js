define(["entity"], function (entity)
{
    entity.createComponent("seek", "move towards the target", "seek", function ()
    {
        return {
            target: 0,
            speed: 200
        };
    });
});
