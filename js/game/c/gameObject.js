define(["entity"], function (entity)
{
    entity.createComponent("gameObject", "marks this entity as an game object (to be deleted when we load a new level)",
        "gameObject", function ()
        {
            return {};
        });
});
