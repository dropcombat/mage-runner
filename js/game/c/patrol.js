define(["entity"], function (entity)
{
    entity.createComponent("patrol", "patrols between two y values a and b", "patrol", function ()
    {
        return {
            a: 0,
            b: 0,
            speed: 200
        };
    });
});
