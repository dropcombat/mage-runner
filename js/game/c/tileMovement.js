define(["entity"], function (entity)
{
    entity.createComponent("tileMovement", "movement for the background tiles", "tileMovement", function ()
    {
        return {
            x: 0,
            y: 0
        };
    });
});
