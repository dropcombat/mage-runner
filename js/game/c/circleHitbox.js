define(["entity"], function (entity)
{
    entity.createComponent("circleHitbox", "hitbox with a radius!", "circleHitbox", function ()
    {
        return {
            r: 0,
            team: false
        };
    });
});
