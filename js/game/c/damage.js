define(["entity"], function (entity)
{
    entity.createComponent("damage", "how much damage this should deal", "damage", function ()
    {
        return {
            damage: 100
        };
    });
});
