define(["entity"], function (entity)
{
    entity.createComponent("smartBomb", "count of smart bombs", "smartBomb", function ()
    {
        return {
            count: 3
        };
    });
});
