define(["entity"], function (entity)
{
    entity.createComponent("charger", "charges at the hero", "charger", function ()
    {
        return {
            chargeVelocity: -800,
            walkVelocity: -200,
            sensorRange: 90
        };
    });
});
