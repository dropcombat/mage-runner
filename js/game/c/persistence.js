define(["entity"], function (entity)
{
    entity.createComponent("persistence", "link to entity that maintains persistence", "persistence", function ()
    {
        return {
            dataEntityId: 0
        };
    });
});
