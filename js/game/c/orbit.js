define(["entity"], function (entity)
{
    entity.createComponent("orbit", "defines sinusoidal movement around a center entity", "orbit", function ()
    {
        return {
            centerId: 0,
            amplitudeX: 0,
            periodX: 1000,
            shiftX: 0,
            amplitudeY: 0,
            periodY: 1000,
            shiftY: 0
        };
    });
});
