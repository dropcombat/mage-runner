define(["entity"], function (entity)
{
    entity.createComponent("health", "0 health = dead", "health", function ()
    {
        return {
            current: 100,
            max: 100
        };
    });
});
