define(["entity"], function (entity)
{
    entity.createComponent("collidable", "this entity can collide with a circleHitbox", "collidable", function ()
    {
        return {
            trigger: 0
        };
    });
});
