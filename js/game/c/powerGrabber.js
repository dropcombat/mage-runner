define(["entity"], function (entity)
{
    entity.createComponent("powerGrabber", "this entity can use power ups", "powerGrabber", function ()
    {
        return {
            speed: {
                current: 1,
                duration: 0,
                defaultValue: 1
            },
            multi: {
                current: 0,
                duration: 0,
                defaultValue: 0
            }
        };
    });
});
