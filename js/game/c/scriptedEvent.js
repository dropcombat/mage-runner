define(["entity"], function (entity)
{
    entity.createComponent("scriptedEvent", "a scripted event used to create a level", "scriptedEvent", function ()
    {
        return {
            timestamp: 0,
            type: "spawn",
            params: []
        };
    });
});
