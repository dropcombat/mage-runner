define(["entity"], function (entity)
{
    entity.createComponent("ranged", "fired projectiles", "ranged", function ()
    {
        return {
            chargeTime: 1000,
            xOffset: 0,
            yOffset: 0,
            xVelocity: 0,
            yVelocity: 0,
            homingTarget: 0,
            homingSpeed: 0,
            image: "fireball.png",
            imageWidth: 36,
            imageHeight: 36,
            centerXPercentage: 0.5,
            centerYPercentage: 0.5,
            team: "",
            r: 18,
            damage: 50,
            onHit: "",
            onHitData: null
        };
    });
});
