define(["entity", "core/message", "render/sys/render", "core/gameLoop", "audio/util/sound", "game/hero",
        "game/levelCollection", "input/keyListener", "storage/local"],
    function (entity, message, renderSystem, gameLoop, sound, heroHelper, levelCollection, keyListener, storage)
    {
        // contains basic main menu elements
        var mainMenus = [];
        // contains the rest of the menu elements
        var hideMenus = [];
        var xOffset = 915;
        var healthBar;
        var logo = "logo.png";
        var started = false;
        var running = false;
        var selectedDifficulty = 0;
        var selectedLevel;

        var menuTextY = -450;
        var hudY = 495;
        var fontSize = 50;
        var shopGoldText;
        // index with the current level to get the upgrade cost
        // [0] will never be used
        var upgradeCosts = [0, 10, 50, 100, 250, 500, 800, 1500, 2500, 5000];

        message.addListener("toggleMusic", function ()
        {
            sound.toggleBackground();
            menuHelper.hideMenu(false);
            menuHelper.optionsMenu();
        });

        message.addListener("toggleSfx", function (msg)
        {
            sound.setPlaySfx(msg.data);
            menuHelper.hideMenu(false);
            menuHelper.optionsMenu();
        });

        message.addListener("showMenu", function (msg)
        {
            if (msg.data)
            {
                menuHelper.hideMenu(false);
                switch (msg.data.menu)
                {
                case "shop":
                    menuHelper.shopMenu();
                    break;
                case "options":
                    menuHelper.optionsMenu();
                    break;
                case "instructions":
                    menuHelper.instructionsMenu();
                    break;
                case "credits":
                    menuHelper.creditsMenu();
                    break;
                case "levelSelect":
                    menuHelper.levelMenu();
                    break;
                default:
                    console.log("Undefined menu called");
                    break;
                }
            }
            else
            {
                // no specific menu to show, show the main menu instead
                menuHelper.mainMenu();
                gameLoop.pause();
            }
        });

        message.addListener("hideMenu", function (msg)
        {
            menuHelper.hideMenu(msg.data.mode);
            if (msg.data.mode)
            {
                gameLoop.start();

            }
        });

        message.addListener("startGame", function ()
        {
            if (!started)
            {
                started = true;
                menuHelper.createHUD();
            }
            if (!running)
            {
                running = true;
                levelCollection.loadDefault();
            }
            menuHelper.hideMenu(true);
            gameLoop.start();
        });

        keyListener.press(keyListener.P, function ()
        {
            if (running)
            {
                if (gameLoop.getPause())
                {
                    // is paused, unpause it
                    menuHelper.hideMenu(true);
                    gameLoop.start();
                }
                else
                {
                    // game is not paused, pause it.
                    gameLoop.pause();
                    menuHelper.showPause();
                }
            }
        });

        message.addListener("death", function (msg)
        {
            if (msg.id == heroHelper.getHeroId())
            {
                menuHelper.updateHealthDisplay();
                // game over
                running = false;
                gameLoop.pause();
                menuHelper.showGameOver();
            }
        });

        message.addListener("selectDifficulty", function (msg)
        {
            selectedDifficulty = msg.data;
            menuHelper.hideMenu(false);
            menuHelper.levelMenu();

        });

        message.addListener("loadSelectedLevel", function ()
        {
            if (selectedDifficulty >= 0 && selectedLevel)
            {
                levelCollection.load(selectedLevel, selectedDifficulty);
                if (!started)
                {
                    started = true;
                    menuHelper.createHUD();
                }
                if (!running)
                {
                    running = true;
                }
                menuHelper.hideMenu(true);
                gameLoop.start();
            }
            // debug purposes
            else
            {
                console.log(selectedDifficulty + " " + selectedLevel);
            }
        });

        message.addListener("selectLevel", function (msg)
        {
            selectedLevel = msg.data;
            selectedDifficulty = 0;
            menuHelper.hideMenu(false);
            menuHelper.levelMenu();
        });

        message.addListener("levelComplete", function ()
        {
            running = false;
            gameLoop.pause();
            menuHelper.showLevelComplete();
        });

        message.addListener("nextLevel", function ()
        {
            running = true;
            levelCollection.loadNext();
            menuHelper.hideMenu(true);
            gameLoop.start();
        });

        message.addListener("buyUpgrade", function (msg)
        {
            var upgradeType = msg.data.upgrade;
            var level = heroHelper.getUpgradeLevel(upgradeType);
            var cost = upgradeCosts[level];
            var gold = storage.get("gold") || 0;
            // check for gold and whatnot here
            if (level < 10 && gold >= cost)
            {
                level++;
                var newGold = gold - cost;
                var goldData = entity.getComponentDataForEntity("gold", heroHelper.getPersistentDataId());
                goldData.current = newGold;
                storage.set("gold", newGold);
                heroHelper.setUpgradeLevel(upgradeType, level);
                setUpgradeLevel(upgradeType, msg.data.barId, msg.data.costId);
                updateShopGoldText();
                menuHelper.updateGoldCount();
            }
        });

        function displayUpgrade(type, y)
        {
            var data;

            var costText = menuHelper.createText(type + "UpgradeCostText", 400, y, 250, 40, "white", 40, 5005, "");
            hideMenus.push(costText);

            var upgradeBarOutline = menuHelper.createMenu(type + "UpgradeBarOutline", 0, y, 400, 40, null, "#fff",
                false);
            data = entity.getComponentDataForEntity("shape", upgradeBarOutline);
            data.zIndex = 5006;
            hideMenus.push(upgradeBarOutline);

            var upgradeBar = menuHelper.createMenu(type + "UpgradeBar", 0, y, 400, 40, "#ff0000", null, false);
            data = entity.getComponentDataForEntity("shape", upgradeBar);
            data.zIndex = 5005;
            hideMenus.push(upgradeBar);

            var upgradeText = menuHelper.createText(type + "UpgradeText", -350, y, 250, 40, "white", 40, 5005, type);
            entity.addComponentToEntity("clickable", upgradeText);
            data = entity.getComponentDataForEntity("clickable", upgradeText);
            data.width = 250;
            data.height = 40;
            data.message = "buyUpgrade";
            data.data = {upgrade: type, barId: upgradeBar, costId: costText};
            data.zIndex = 5005;
            entity.refreshEntity(upgradeText);
            hideMenus.push(upgradeText);

            setUpgradeLevel(type, upgradeBar, costText);
        }

        function setUpgradeLevel(type, barId, costId)
        {
            var level = heroHelper.getUpgradeLevel(type);
            var goldCost = upgradeCosts[level];
            var data = entity.getComponentDataForEntity("shape", barId);
            data.width = 40 * level;
            data = entity.getComponentDataForEntity("position", barId);
            data.x = xOffset - 200 + 20 * level;
            data = entity.getComponentDataForEntity("text", costId);
            data.text = goldCost ? goldCost + " Gold" : "";
        }

        function updateShopGoldText()
        {
            var data = entity.getComponentDataForEntity("text", shopGoldText);
            data.text = (storage.get("gold") || 0) + " Gold";
        }

        var menuHelper = {
            /**
             * @param entName Entity name
             * @param x X Coordinate
             * @param y Y Coordinate
             * @param width Width
             * @param height Height
             * @param fill Fill color
             * @param stroke Stroke color
             * @param hide Hide this box when menu closes?
             * @return {entity}
             */
            createMenu: function (entName, x, y, width, height, fill, stroke, hide)
            {
                var ent = entity.createEntity(entName);
                entity.addComponentToEntity("position", ent);
                var d = entity.getComponentDataForEntity("position", ent);
                d.x = xOffset + x;
                d.y = y;
                entity.addComponentToEntity("shape", ent);
                d = entity.getComponentDataForEntity("shape", ent);
                d.width = width;
                d.height = height;
                d.fill = fill;
                d.stroke = stroke;
                entity.refreshEntity(ent);
                if (hide !== false)
                {
                    hideMenus.push(ent);
                }
                return ent;
            },
            /**
             * @param entName Entity name
             * @param x X Coordinate
             * @param y Y Coordinate
             * @param width Width
             * @param height height
             * @param fill Fill color
             * @param size Text Size
             * @param zIndex zIndex for the text
             * @param text Text to insert
             * @param hide Hide this text when menu closes?
             * @return {entity}
             */
            createText: function (entName, x, y, width, height, fill, size, zIndex, text, hide)
            {
                var ent = entity.createEntity(entName);
                entity.addComponentToEntity("position", ent);
                var d = entity.getComponentDataForEntity("position", ent);
                d.x = xOffset + x;
                d.y = y;
                entity.addComponentToEntity("text", ent);
                d = entity.getComponentDataForEntity("text", ent);
                d.width = width;
                d.height = height;
                d.text = text;
                d.fill = fill;
                d.fontSize = size;
                d.zIndex = zIndex;
                entity.refreshEntity(ent);
                if (hide !== false)
                {
                    hideMenus.push(ent);
                }
                return ent;
            },
            mainMenu: function ()
            {
                var data, ent;
                var startText = running ? "Resume Game" : "Start Game";

                ent = this.createMenu("mainMenu", 0, 0, 1920, 1080, "#000", null, false);
                data = entity.getComponentDataForEntity("shape", ent);
                data.strokeWidth = 0;
                data.zIndex = 5000;
                entity.refreshEntity(ent);
                mainMenus.push(ent);

                //noinspection SpellCheckingInspection
                ent = entity.createEntity("mageRunnerLogo");
                entity.addComponentToEntity("image", ent);
                data = entity.getComponentDataForEntity("image", ent);
                //noinspection SpellCheckingInspection
                data.src = "magerunner.png";
                data.width = 825;
                data.height = 105;
                data.zIndex = 5003;
                entity.addComponentToEntity("position", ent);
                data = entity.getComponentDataForEntity("position", ent);
                data.y = 250;
                data.x = xOffset;
                entity.refreshEntity(ent);
                mainMenus.push(ent);

                ent = this.createText("startText", -750, menuTextY, 300, fontSize, "white", fontSize, 5002, startText,
                    false);
                entity.addComponentToEntity("clickable", ent);
                data = entity.getComponentDataForEntity("clickable", ent);
                data.width = 300;
                data.height = fontSize;
                data.message = "startGame";
                data.zIndex = 5004;
                entity.refreshEntity(ent);
                mainMenus.push(ent);

                var loadColor = storage.get("unlockedLevels") ? "white" : "gray";

                ent = this.createText("loadText", -350, menuTextY, 250, fontSize, loadColor, fontSize, 5002,
                    "Level Select", false);
                if (loadColor == "white")
                {
                    entity.addComponentToEntity("clickable", ent);
                    data = entity.getComponentDataForEntity("clickable", ent);
                    data.width = 250;
                    data.height = fontSize;
                    data.message = "showMenu";
                    data.data = {menu: "levelSelect"};
                    data.zIndex = 5004;
                }
                entity.refreshEntity(ent);
                mainMenus.push(ent);

                ent = this.createText("shopText", -50, menuTextY, 100, fontSize, "white", fontSize, 5002, "Shop",
                    false);
                entity.addComponentToEntity("clickable", ent);
                data = entity.getComponentDataForEntity("clickable", ent);
                data.width = 200;
                data.height = fontSize;
                data.message = "showMenu";
                data.data = {menu: "shop"};
                data.zIndex = 5004;
                entity.refreshEntity(ent);
                mainMenus.push(ent);

                ent = this.createText("optionsText", 200, menuTextY, 200, fontSize, "white", fontSize, 5002, "Options",
                    false);
                entity.addComponentToEntity("clickable", ent);
                data = entity.getComponentDataForEntity("clickable", ent);
                data.width = 200;
                data.height = fontSize;
                data.message = "showMenu";
                data.data = {menu: "options"};
                data.zIndex = 5004;
                entity.refreshEntity(ent);
                mainMenus.push(ent);

                ent = this.createText("instructionsText", 500, menuTextY, 250, fontSize, "white", fontSize, 5002,
                    "Instructions", false);
                entity.addComponentToEntity("clickable", ent);
                data = entity.getComponentDataForEntity("clickable", ent);
                data.width = 250;
                data.height = fontSize;
                data.message = "showMenu";
                data.data = {menu: "instructions"};
                data.zIndex = 5004;
                entity.refreshEntity(ent);
                mainMenus.push(ent);

                ent = this.createText("creditsText", 800, menuTextY, 150, fontSize, "white", fontSize, 5002, "Credits",
                    false);
                entity.addComponentToEntity("clickable", ent);
                data = entity.getComponentDataForEntity("clickable", ent);
                data.width = 150;
                data.height = fontSize;
                data.message = "showMenu";
                data.data = {menu: "credits"};
                data.zIndex = 5004;
                entity.refreshEntity(ent);
                mainMenus.push(ent);

            },
            optionsMenu: function ()
            {
                var ent, data;
                // music toggle Box
                ent = this.createMenu("optionsMenu", 0, 0, 300, 300, "#1a1a1a", "#550000");
                data = entity.getComponentDataForEntity("shape", ent);
                data.zIndex = 5004;
                data.cornerRadius = 10;
                hideMenus.push(ent);

                ent = this.createText("optionsText", 0, 120, 250, 40, "white", 30, 5002, "Options");
                hideMenus.push(ent);

                ent = this.createText("optionsCloseText", 145, 135, 40, 40, "red", 35, 5002, "x");
                entity.addComponentToEntity("clickable", ent);
                data = entity.getComponentDataForEntity("clickable", ent);
                data.width = 40;
                data.height = 40;
                data.message = "hideMenu";
                data.data = {mode: false};
                data.zIndex = 5005;
                entity.refreshEntity(ent);
                hideMenus.push(ent);

                // Text for music toggle box
                var state = sound.getPlayBackground() ? "Off" : "On";
                var t = "Toggle music " + state;
                ent = this.createText("musicToggleText", 0, 50, 250, 40, "red", 25, 5002, t);
                entity.addComponentToEntity("clickable", ent);
                data = entity.getComponentDataForEntity("clickable", ent);
                data.width = 250;
                data.height = 40;
                data.message = "toggleMusic";
                data.zIndex = 5005;
                entity.refreshEntity(ent);
                hideMenus.push(ent);

                // Text for SFX toggle
                state = sound.getPlaySfx() ? "Off" : "On";
                t = "Toggle Sound effects " + state;
                ent = this.createText("sfXToggleText", 0, 0, 250, 40, "red", 25, 5002, t);
                entity.addComponentToEntity("clickable", ent);
                data = entity.getComponentDataForEntity("clickable", ent);
                data.width = 250;
                data.height = 40;
                data.message = "toggleSfx";
                data.data = !sound.getPlaySfx();
                data.zIndex = 5005;
                entity.refreshEntity(ent);
                hideMenus.push(ent);

                // create a blocker over the button to stop multiple menus from being created
                ent = this.createMenu("optionsMenuBlocker", -25, -250, 150, 75, null, null);
                data = entity.getComponentDataForEntity("shape", ent);
                data.zIndex = 5004;
                data.strokeWidth = 0;
                entity.addComponentToEntity("clickable", ent);
                data = entity.getComponentDataForEntity("clickable", ent);
                data.width = 250;
                data.height = 40;
                data.message = "theBackingDoesNothing";
                data.zIndex = 5005;
                entity.refreshEntity(ent);
                hideMenus.push(ent);

            },
            levelMenu: function ()
            {
                var names;
                try
                {
                    names = JSON.parse(storage.get("unlockedLevels"));
                }
                catch (e)
                {
                    console.log("unable to load unlocked levels: " + e);
                    names = "";
                }

                var startY = 175;

                var ent, data;
                ent = this.createMenu("levelMenu", 0, 0, 1050, 768, "#1a1a1a", "#550000");
                data = entity.getComponentDataForEntity("shape", ent);
                data.zIndex = 5004;
                data.cornerRadius = 10;
                hideMenus.push(ent);

                ent = this.createText("levelSelectText", -375, 350, 250, 40, "white", 30, 5002, "Level Select");
                hideMenus.push(ent);

                ent = this.createText("unlockedLevelsText", -375, 230, 250, 40, "white", 30, 5002, "Unlocked Levels:");
                hideMenus.push(ent);

                ent = this.createText("levelSelectCloseText", 510, 365, 40, 40, "red", 35, 5002, "x");
                entity.addComponentToEntity("clickable", ent);
                data = entity.getComponentDataForEntity("clickable", ent);
                data.width = 40;
                data.height = 40;
                data.message = "hideMenu";
                data.data = {mode: false};
                data.zIndex = 5005;
                entity.refreshEntity(ent);
                hideMenus.push(ent);
                var fontColor;

                // Look parse which levels have been unlocked and display them.
                for (var ndx = 0; ndx < names.length; ndx++)
                {
                    var split = names[ndx].split("|");
                    // Only display the levels once per difficulty, level progression should be displayed properly unlesss cheating
                    if (split[1] == "0")
                    {

                        if (selectedLevel == split[0])
                        {
                            fontColor = "white";
                        }
                        else
                        {
                            fontColor = "#666";
                        }
//                        console.log(split[0]);
                        ent = this.createText(split[0] + "SelectText", -375, startY, 250, fontSize, fontColor, 30, 5002,
                            split[0]);
                        entity.addComponentToEntity("clickable", ent);
                        data = entity.getComponentDataForEntity("clickable", ent);
                        data.width = 250;
                        data.height = fontSize;
                        data.message = "selectLevel";
                        data.data = split[0];
                        entity.refreshEntity(ent);
                        startY -= fontSize;
                    }
                }
                if (selectedLevel)
                {
                    this.showDifficulty();
                }
                ent = this.createMenu("loadButton", 0, -320, 150, 60, "#1a1a1a", "#550000");
                entity.addComponentToEntity("clickable", ent);
                data = entity.getComponentDataForEntity("clickable", ent);
                data.width = 100;
                data.height = fontSize;
                data.message = "loadSelectedLevel";
                data = entity.getComponentDataForEntity("shape", ent);
                data.zIndex = 6002;
                data.cornerRadius = 10;
                entity.refreshEntity(ent);

                this.createText("menuButtonText", 0, -320, 110, fontSize, "#c8b7b7", fontSize, 6003, "Load!");
            },
            showDifficulty: function ()
            {
                var selectedDifficulties = [];
                var names = JSON.parse(storage.get("unlockedLevels"));

                for (var ndx = 0; ndx < names.length; ndx++)
                {
                    var split = names[ndx].split("|");
                    if (split[0] == selectedLevel)
                    {
                        selectedDifficulties.push(split[1]);
                    }
                }

                var ent, data;
                ent = this.createText("difficultySelectText", 175, 230, 250, 40, "white", 30, 5002,
                    "Select Difficulty:");
                hideMenus.push(ent);
                for (var index = 0; index < selectedDifficulties.length; index++)
                {
                    if (selectedDifficulties[index] == "0")
                    {
                        ent = this.createText("bronzeDifficultySelectText", 0, 160, 75, 40, this.getDifficultyColor(0),
                            30, 5002, "Bronze");
                        entity.addComponentToEntity("clickable", ent);
                        data = entity.getComponentDataForEntity("clickable", ent);
                        data.width = 75;
                        data.height = fontSize;
                        data.message = "selectDifficulty";
                        data.data = 0;
                        data.zIndex = 6000;
                        entity.refreshEntity(ent);
                        hideMenus.push(ent);
                    }
                    if (selectedDifficulties[index] == "1")
                    {
                        ent = this.createText("silverDifficultySelectText", 150, 160, 75, 40,
                            this.getDifficultyColor(1), 30, 5002, "Silver");
                        entity.addComponentToEntity("clickable", ent);
                        data = entity.getComponentDataForEntity("clickable", ent);
                        data.width = 75;
                        data.height = fontSize;
                        data.message = "selectDifficulty";
                        data.data = 1;
                        data.zIndex = 6000;
                        entity.refreshEntity(ent);
                        hideMenus.push(ent);
                    }
                    if (selectedDifficulties[index] == "2")
                    {
                        ent = this.createText("goldDifficultySelectText", 300, 160, 75, 40, this.getDifficultyColor(2),
                            30, 5002, "Gold");
                        entity.addComponentToEntity("clickable", ent);
                        data = entity.getComponentDataForEntity("clickable", ent);
                        data.width = 75;
                        data.height = fontSize;
                        data.message = "selectDifficulty";
                        data.data = 2;
                        data.zIndex = 6000;
                        entity.refreshEntity(ent);
                        hideMenus.push(ent);
                    }
                }

            },
            getDifficultyColor: function (difficultyNum)
            {
                if (selectedDifficulty == difficultyNum)
                {
                    return "white";
                }
                else
                {
                    return "#666";
                }
            },
            shopMenu: function ()
            {
                var ent, data;
                ent = this.createMenu("shopMenu", 0, -75, 1000, 450, "#1a1a1a", "#550000");
                data = entity.getComponentDataForEntity("shape", ent);
                data.zIndex = 5004;
                data.cornerRadius = 10;
                hideMenus.push(ent);

                ent = this.createText("shopText", -350, 120, 250, 40, "white", 40, 5005, "Shop");
                hideMenus.push(ent);

                ent = this.createText("goldText", 0, 120, 250, 40, "white", 40, 5005, "");
                hideMenus.push(ent);
                shopGoldText = ent;

                ent = this.createText("shopCloseText", 495, 135, 40, 40, "red", 35, 5005, "x");
                entity.addComponentToEntity("clickable", ent);
                data = entity.getComponentDataForEntity("clickable", ent);
                data.width = 40;
                data.height = 40;
                data.message = "hideMenu";
                data.data = {mode: false};
                data.zIndex = 5005;
                entity.refreshEntity(ent);
                hideMenus.push(ent);

                // upgrades
                updateShopGoldText();
                displayUpgrade("Damage", 0);
                displayUpgrade("Cast Speed", -80);
                displayUpgrade("Health", -160);
                displayUpgrade("Movement", -240);

            },
            instructionsMenu: function ()
            {
                var ent, data;

                ent = this.createMenu("instructionsMenu", 0, 20, 650, 500, "#1a1a1a", "#550000");
                data = entity.getComponentDataForEntity("shape", ent);
                data.zIndex = 5006;
                data.cornerRadius = 10;
                hideMenus.push(ent);

                ent = this.createText("instructionsText", -180, 245, 250, 40, "white", 30, 5002, "Instructions");
                hideMenus.push(ent);

                ent = this.createText("instructionsCloseText", 310, 250, 40, 40, "red", 35, 5002, "x");
                entity.addComponentToEntity("clickable", ent);
                data = entity.getComponentDataForEntity("clickable", ent);
                data.width = 40;
                data.height = 40;
                data.message = "hideMenu";
                data.data = {mode: false};
                data.zIndex = 5005;
                entity.refreshEntity(ent);
                hideMenus.push(ent);

                //noinspection SpellCheckingInspection
                var t = "You are a Mage!\n\nUse W, A, S, and D to move your character, he will fire automatically.\n\n"
                            + "Dodge and destroy the enemies\n\nPress comma ( , ) to use a smart-bomb that destroys all enemies on the screen"
                            + "except for bosses\n\n Press period ( . ) to change your fire mode. The alternate fire mode will"
                    + " pierce enemies, but only does half damage.\n\nPress P to pause the game\n\nGood Luck!";
                ent = this.createText("instructionsContentText", -10, 155, 600, 70, "white", 25, 5002, t);
                hideMenus.push(ent);

                // create a blocker over the button to stop multiple menus from being created
                ent = this.createMenu("instructionsMenuBlocker", 250, -250, 150, 75, null, null);
                data = entity.getComponentDataForEntity("shape", ent);
                data.zIndex = 5004;
                data.strokeWidth = 0;
                entity.addComponentToEntity("clickable", ent);
                data = entity.getComponentDataForEntity("clickable", ent);
                data.width = 250;
                data.height = 40;
                data.message = "theBackingDoesNothing";
                data.zIndex = 5005;
                entity.refreshEntity(ent);
                hideMenus.push(ent);
            },
            creditsMenu: function ()
            {
                var ent, data;

                ent = this.createMenu("creditsMenu", 0, 20, 650, 500, "#1a1a1a", "#550000");
                data = entity.getComponentDataForEntity("shape", ent);
                data.zIndex = 5006;
                data.cornerRadius = 10;
                hideMenus.push(ent);

                ent = this.createText("creditsText", -180, 245, 250, 40, "white", 30, 5002, "Credits");
                hideMenus.push(ent);

                ent = this.createText("creditsCloseText", 310, 250, 40, 40, "red", 35, 5002, "x");
                entity.addComponentToEntity("clickable", ent);
                data = entity.getComponentDataForEntity("clickable", ent);
                data.width = 40;
                data.height = 40;
                data.message = "hideMenu";
                data.data = {mode: false};
                data.zIndex = 5005;
                entity.refreshEntity(ent);
                hideMenus.push(ent);

                //noinspection SpellCheckingInspection
                var t = "Mage Runner was created by the team at www.dropcombat.com:\n\n"
                            + "Thomas \"HazedTruth\" Dickens - Lead Programmer, Art"
                            + "\n\nBrandon \"SickDisturbence\" Agbalog - Programmer"
                            + "\n\nPeter \"SilenceDoGood\" Bue - SFX" + "\n\nSpecial thanks to:"
                    + "\n\nJack \"Chucky\" Angus - title image\nhttp://elskullkido.deviantart.com/" +
                        "\n\nDST - Music\nhttp://www.nosoapradio.us/";

                ent = this.createText("creditsContentText", -10, 155, 600, 70, "white", 25, 5002, t);
                hideMenus.push(ent);

                // create a blocker over the button to stop multiple menus from being created
                ent = this.createMenu("creditsMenuBlocker", 475, -250, 150, 75, null, null);
                data = entity.getComponentDataForEntity("shape", ent);
                data.zIndex = 5004;
                data.strokeWidth = 0;
                entity.addComponentToEntity("clickable", ent);
                data = entity.getComponentDataForEntity("clickable", ent);
                data.width = 250;
                data.height = 40;
                data.message = "theBackingDoesNothing";
                data.zIndex = 5005;
                entity.refreshEntity(ent);
                hideMenus.push(ent);
            },
            hideMenu: function (hideMainMenu)
            {

                // Delete all the menu items stored in menu holder.
                for (var m in hideMenus)
                {
                    entity.deleteEntity(hideMenus[m]);
                }

                if (hideMainMenu)
                {
                    for (var mm in mainMenus)
                    {
                        entity.deleteEntity(mainMenus[mm]);
                    }
                }

            },
            showBacking: function ()
            {
                var data;
                var backing = entity.createEntity("backing");
                entity.addComponentToEntity("clickable", backing);
                data = entity.getComponentDataForEntity("clickable", backing);
                data.width = 1920;
                data.height = 1080;
                data.message = "theBackingDoesNothing";
                data.zIndex = 4999;
                // Menu backing intercepts clicks, but performs no trigger.
                entity.addComponentToEntity("position", backing);
                data = entity.getComponentDataForEntity("position", backing);
                data.x = xOffset;
                data.y = 0;
                entity.addComponentToEntity("shape", backing);
                data = entity.getComponentDataForEntity("shape", backing);
                data.width = 1920;
                data.height = 1080;
                data.zIndex = 4999;
                data.fill = "black";
                data.stroke = "black";
                data.strokeWidth = 0;
                data.opacity = 0.7;
                entity.refreshEntity(backing);
                hideMenus.push(backing);
            },
            createMenuButton: function ()
            {
                var ent = this.createMenu("menuButton", -860, hudY, 175, 60, "#1a1a1a", "#550000", false);
                entity.addComponentToEntity("clickable", ent);
                var data = entity.getComponentDataForEntity("clickable", ent);
                data.width = 100;
                data.height = fontSize;
                data.message = "showMenu";
                entity.refreshEntity(ent);

                // text for menu button
                this.createText("menuButtonText", -850, hudY, 150, fontSize, "#c8b7b7", fontSize, 4001, "Menu", false);
            },
            createSmartBombCount: function ()
            {
                this.createMenu("bombBackground", -170, hudY, 250, 60, "#1a1a1a", "#550000", false);

                // text for menu button
                this.bombTextEntity = this.createText("bombText", -160, hudY, 230, fontSize, "#c8b7b7", fontSize, 4001,
                    "Bombs: ", false);
                this.updateSmartBombCount();
            },
            updateSmartBombCount: function ()
            {
                if (running)
                {
                    var smartBombData = entity.getComponentDataForEntity("smartBomb", heroHelper.getHeroId());
                    var value = smartBombData ? smartBombData.count : "";
                    entity.getComponentDataForEntity("text", this.bombTextEntity).text = "Bombs: " + value;
                }
            },
            createGoldCount: function ()
            {
                this.createMenu("bombBackground", 100, hudY, 250, 60, "#1a1a1a", "#550000", false);

                // text for menu button
                this.goldTextEntity = this.createText("bombText", 110, hudY, 230, fontSize, "#c8b7b7", fontSize, 4001,
                    "Gold: ", false);
                this.updateSmartBombCount();
            },
            updateGoldCount: function ()
            {
                if (running)
                {
                    var goldData = entity.getComponentDataForEntity("gold", heroHelper.getPersistentDataId());
                    var value = goldData ? goldData.current : "";
                    entity.getComponentDataForEntity("text", this.goldTextEntity).text = "Gold: " + value;
                }
            },
            createHealthDisplay: function ()
            {
                menuHelper.createMenu("healthBarOutline", -520, hudY, 400, fontSize, null, "#fff", false);
                healthBar = menuHelper.createMenu("healthBar", -520, hudY, 400, fontSize, "#ff0000", null, false);
                this.createText("healthBarText", -510, hudY, 150, fontSize, "#fff", fontSize, 4001, "Health", false);
            },
            updateHealthDisplay: function ()
            {
                if (running)
                {
                    var health = entity.getComponentDataForEntity("health", heroHelper.getHeroId());
                    var data = entity.getComponentDataForEntity("shape", healthBar);
                    // check to see if the player is still alive
                    if (health)
                    {
                        // convert the damage into width scaled by the heros max health.
                        data.width = health.current / health.max * 400;
                    }
                    else
                    {
                        // player is dead, set health bar to 0
                        data.width = 0;
                    }
                    if (data.width < 0)
                    {
                        data.width = 0;
                    }
                }
            },
            createHUD: function ()
            {
                this.createMenuButton();
                this.createSmartBombCount();
                this.createGoldCount();
                this.createHealthDisplay();
            },
            showPause: function ()
            {
                this.showBacking();
                var pauseMenu = this.createMenu("pauseMenu", 0, 0, 410, 175, "#1a1a1a", "#550000");
                var data = entity.getComponentDataForEntity("shape", pauseMenu);
                data.zIndex = 5001;
                data.cornerRadius = 10;
                data.strokeWidth = 10;
                entity.refreshEntity(pauseMenu);
                hideMenus.push(pauseMenu);

                var t = "          Paused\n\nPress 'P' to resume.";
                var pauseText = this.createText("pauseMenuText", 35, -53, 390, 245, "#c8b7b7", 20, 5002, t);
                data = entity.getComponentDataForEntity("text", pauseText);
                data.fontSize = 40;
                entity.refreshEntity(pauseText);
                hideMenus.push(pauseText);
            },
            showGameOver: function ()
            {
                this.showBacking();
                var gameOverMenu = this.createMenu("gameOverMenu", 0, 0, 510, 175, "#1a1a1a", "#550000");
                var data = entity.getComponentDataForEntity("shape", gameOverMenu);
                data.cornerRadius = 10;
                data.strokeWidth = 10;
                data.zIndex = 5001;
                entity.refreshEntity(gameOverMenu);
                hideMenus.push(gameOverMenu);

                var t = "          Game Over\n\nBetter luck next time!";
                var gameOverText = this.createText("gameOverText", 15, -53, 390, 245, "#c8b7b7", 20, 5002, t);
                data = entity.getComponentDataForEntity("text", gameOverText);
                data.fontSize = 40;
                entity.refreshEntity(gameOverText);
                hideMenus.push(gameOverText);

                var retryText = this.createText("retryText", -180, -200, 130, fontSize, "#c8b7b7", fontSize, 5002,
                    "Retry", false);
                entity.addComponentToEntity("clickable", retryText);
                data = entity.getComponentDataForEntity("clickable", retryText);
                data.width = 130;
                data.height = fontSize;
                data.message = "startGame";
                data.zIndex = 5002;
                entity.refreshEntity(retryText);
                hideMenus.push(retryText);

                var quitText = this.createText("quitText", 200, -200, 130, fontSize, "#c8b7b7", fontSize, 5002, "Quit",
                    false);
                entity.addComponentToEntity("clickable", quitText);
                data = entity.getComponentDataForEntity("clickable", quitText);
                data.width = 130;
                data.height = fontSize;
                data.message = "showMenu";
                data.zIndex = 5002;
                entity.refreshEntity(quitText);
                hideMenus.push(quitText);
            },
            showLevelComplete: function ()
            {
                this.showBacking();
                var gameOverMenu = this.createMenu("gameOverMenu", 0, 0, 510, 175, "#1a1a1a", "#550000");
                var data = entity.getComponentDataForEntity("shape", gameOverMenu);
                data.cornerRadius = 10;
                data.strokeWidth = 10;
                data.zIndex = 5001;
                entity.refreshEntity(gameOverMenu);
                hideMenus.push(gameOverMenu);

                var t = "       Level Complete\n\n     Great job mage!";
                var levelCompleteMenu = this.createText("levelCompleteMenu", 15, -53, 390, 245, "#c8b7b7", 20, 5002, t);
                data = entity.getComponentDataForEntity("text", levelCompleteMenu);
                data.fontSize = 40;
                entity.refreshEntity(levelCompleteMenu);
                hideMenus.push(levelCompleteMenu);

                var replayText = this.createText("replayText", -180, -200, 130, fontSize, "#c8b7b7", fontSize, 5002,
                    "Replay", false);
                entity.addComponentToEntity("clickable", replayText);
                data = entity.getComponentDataForEntity("clickable", replayText);
                data.width = 130;
                data.height = fontSize;
                data.message = "startGame";
                data.zIndex = 5002;
                entity.refreshEntity(replayText);
                hideMenus.push(replayText);

                var nextText = this.createText("nextText", 30, -200, 130, fontSize, "#c8b7b7", fontSize, 5002, "Next",
                    false);
                entity.addComponentToEntity("clickable", nextText);
                data = entity.getComponentDataForEntity("clickable", nextText);
                data.width = 130;
                data.height = fontSize;
                data.message = "nextLevel";
                data.zIndex = 5002;
                entity.refreshEntity(nextText);
                hideMenus.push(nextText);

                var quitText = this.createText("quitText", 200, -200, 130, fontSize, "#c8b7b7", fontSize, 5002, "Quit",
                    false);
                entity.addComponentToEntity("clickable", quitText);
                data = entity.getComponentDataForEntity("clickable", quitText);
                data.width = 130;
                data.height = fontSize;
                data.message = "showMenu";
                data.zIndex = 5002;
                entity.refreshEntity(quitText);
                hideMenus.push(quitText);

            }
        };

        return menuHelper;
    });
