define(["entity", "core/gameLoop", "core/message"], function (entity, gameLoop, message)
{
    var offScreenXSpawn = 2000;
    var currentDifficulty = 0;

    message.addListener("difficulty", function (msg)
    {
        currentDifficulty = msg.value;
        if (currentDifficulty !== 0 && currentDifficulty != 1 && currentDifficulty != 2)
        {
            // invalid difficulty set
            currentDifficulty = 0;
        }
    });

    message.addListener("pickup", function (msg)
    {
        var powerGrabberData = entity.getComponentDataForEntity("powerGrabber", msg.triggering);
        if (!powerGrabberData)
        {
            return;
        }
        message.queue({type: "sound", id: msg.id, sound: "pickup"});

        switch (msg.data.type)
        {
        case "bomb":
            var smartBombData = entity.getComponentDataForEntity("smartBomb", msg.triggering);
            if (smartBombData)
            {
                smartBombData.count++;
                entity.deleteEntity(msg.id);
            }
            break;
        case "gold":
            var persistenceData = entity.getComponentDataForEntity("persistence", msg.triggering);
            if (persistenceData)
            {
                var goldData = entity.getComponentDataForEntity("gold", persistenceData.dataEntityId);
                if (goldData)
                {
                    // need to make sure we're adding numbers, not strings :)
                    goldData.current = +goldData.current + [1, 4, 15][currentDifficulty];
                    entity.deleteEntity(msg.id);
                }
            }
            break;
        case "health":
            var health = entity.getComponentDataForEntity("health", msg.triggering);
            if (health)
            {
                health.current += msg.data.value * [1, 2.5, 6][currentDifficulty];
                if (health.current > health.max)
                {
                    health.current = health.max;
                }
                entity.deleteEntity(msg.id);
            }
            break;
        case "speed":
            powerGrabberData.speed.duration = msg.data.duration * 1000;
            powerGrabberData.speed.current = msg.data.value;
            entity.deleteEntity(msg.id);
            break;
        case "multi":
            powerGrabberData.multi.duration = msg.data.duration * 1000;
            powerGrabberData.multi.current = msg.data.value;
            entity.deleteEntity(msg.id);
            break;
        default:
            console.log("unsupported powerup trigger: " + msg.data.type);
            break;
        }
    });

    return {
        spawn: function (type, y, value, duration)
        {
            switch (type)
            {
            case "bomb":
            case "health":
            case "speed":
            case "multi":
            case "gold":
                break;
            default:
                console.log("unsupported powerup type: " + type);
                return;
            }

            var powerupEntity = entity.createEntity(type + " pickup");
            entity.addComponentToEntity("gameObject", powerupEntity);
            entity.addComponentToEntity("position", powerupEntity);
            var data = entity.getComponentDataForEntity("position", powerupEntity);
            data.x = offScreenXSpawn;
            data.y = y;
            entity.addComponentToEntity("velocity", powerupEntity);
            data = entity.getComponentDataForEntity("velocity", powerupEntity);
            data.x = -200;
            entity.addComponentToEntity("image", powerupEntity);
            data = entity.getComponentDataForEntity("image", powerupEntity);
            data.src = "powerups/" + type + ".png";
            entity.addComponentToEntity("circleHitbox", powerupEntity);
            data = entity.getComponentDataForEntity("circleHitbox", powerupEntity);
            data.r = 45;
            data.team = "powerup";
            entity.addComponentToEntity("health", powerupEntity);
            entity.addComponentToEntity("collidable", powerupEntity);
            data = entity.getComponentDataForEntity("collidable", powerupEntity);
            data.trigger = powerupEntity;
            entity.addComponentToEntity("trigger", powerupEntity);
            data = entity.getComponentDataForEntity("trigger", powerupEntity);
            data.on = "pickup";
            data.data = {type: type, value: value, duration: duration};
            entity.addComponentToEntity("sounds", powerupEntity);
            data = entity.getComponentDataForEntity("sounds", powerupEntity);
            data.pickup = type == "gold" ? "coin" : "powerup";
            entity.refreshEntity(powerupEntity);
        }
    };

});
