define(["entity"], function (entity)
{
    var sectionTimestamp = 0;
    var lastTimestamp = 0;
    var enemyCount = 0;
    var goldCount = 0;
    var powerupCount = 0;

    //noinspection JSUnusedGlobalSymbols
    var writer = {
        newLevel: function(config)
        {
            sectionTimestamp = 0;
            lastTimestamp = 0;
            enemyCount = 0;
            goldCount = 0;
            powerupCount = 0;
            if (config.tile)
            {
                this.setTile(config.tile);
            }
        },
        endLevel: function()
        {
            writer.section();
            writer.writeEvent(0, "wait");
            writer.writeEvent(0.01, "end");
        },
        section: function(skipSeconds)
        {
            sectionTimestamp = lastTimestamp + (skipSeconds || 0) * 1000;
        },
        writeEvent: function (seconds, type, params)
        {
            var timestamp = seconds * 1000 + sectionTimestamp;
            if (timestamp > lastTimestamp)
            {
                lastTimestamp = timestamp;
            }
            if (type == "spawn")
            {
                enemyCount++;
            }
            if (type == "powerup")
            {
                if (params[0] == "gold")
                {
                    goldCount++;
                }
                else
                {
                    powerupCount++;
                }
            }
            var scriptedEventEntity = entity.createEntity("scripted event " + seconds);
            entity.addComponentToEntity("scriptedEvent", scriptedEventEntity);
            entity.addComponentToEntity("gameObject", scriptedEventEntity);
            var data = entity.getComponentDataForEntity("scriptedEvent", scriptedEventEntity);
            data.timestamp = timestamp;
            data.type = type;
            data.params = params;
            entity.refreshEntity(scriptedEventEntity);
        },
        repeatEvent: function (repeatValues, type, params)
        {
            var endTime = repeatValues.startTime + repeatValues.duration;
            for (var time = repeatValues.startTime, yShift = 0; time <= endTime;
                 time += repeatValues.stepTime, yShift += repeatValues.stepY)
            {
                writeEventWithYShift(time, type, params, yShift);
            }
        },
        multiEvent: function (multiValues, type, params)
        {
            var yShift = -(multiValues.delta * (multiValues.count - 1) / 2);
            for (var count = 1; count <= multiValues.count; count++, yShift += multiValues.delta)
            {
                writeEventWithYShift(multiValues.time, type, params, yShift);
            }
        },
        setTile: function (tileName)
        {

            var backgroundEntity = entity.createEntity("tile");
            entity.addComponentToEntity("gameObject", backgroundEntity);
            entity.addComponentToEntity("position", backgroundEntity);
            var data = entity.getComponentDataForEntity("position", backgroundEntity);
            data.x = 915;
            data.y = 0;
            entity.addComponentToEntity("tileMovement", backgroundEntity);
            data = entity.getComponentDataForEntity("tileMovement", backgroundEntity);
            data.x = -200;
            entity.addComponentToEntity("shape", backgroundEntity);
            data = entity.getComponentDataForEntity("shape", backgroundEntity);
            data.width = 1920;
            data.height = 900;
            data.fill = "";
            data.stroke = "";
            data.strokeWidth = 0;
            data.tile = "tiles/" + tileName + ".png";
            data.zIndex = 0;
            entity.refreshEntity(backgroundEntity);
        },
        getStats: function()
        {
            return [
                "time: " + (lastTimestamp / 1000) + "s",
                "enemies: " + enemyCount,
                "gold: " + goldCount,
                "powerups: " + powerupCount
            ].join("\n");
        }
    };

    return writer;

    function writeEventWithYShift(time, type, params, yShift)
    {
        var paramsCopy = params.slice(0);
        paramsCopy[1] += yShift;
        writer.writeEvent(time, type, paramsCopy);
    }
});
