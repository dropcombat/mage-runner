// to add a level add the level to the dependencies array as well as the namesString
define(["entity", "storage/local", "core/message", "audio/util/sound", "levels/level1", "levels/level2", "levels/level3", "levels/level4"],
    function (entity, storage, message, soundUtil)
    {
        var map = {};
        var names = [];
        for (var ndx = 4; ndx < arguments.length; ndx++)
        {
            var level = arguments[ndx];
            // maps level to arguments, arguments are offset by 4 to allow for entity, storage, message, and sound to be passed in
            map[level.name] = {level: level, index: ndx - 4};
            names.push(level.name);
        }

        function load(name, difficulty)
        {

            storage.set("level", name);
            storage.set("difficulty", difficulty);

            var levelDifficulty = difficulty || 0;

            // On level load, check to see if we've played the level, if not add it to local storage so we can select it later
            var unlockedLevels;
            //noinspection EmptyCatchBlockJS,UnusedCatchParameterJS
            try
            {
                unlockedLevels = JSON.parse(storage.get("unlockedLevels"));
            }
            catch (e)
            {
            }
            if (unlockedLevels)
            {
                var contains = false;

                for (ndx in unlockedLevels)
                {
                    if (unlockedLevels[ndx] == name + "|" + levelDifficulty)
                    {
                        contains = true;
                        //noinspection BreakStatementJS
                        break;

                    }
                }
                if (!contains)
                {
                    // Never played this level, add it.
                    unlockedLevels.push(name + "|" + levelDifficulty);
                    storage.set("unlockedLevels", JSON.stringify(unlockedLevels));
                }
            }
            else
            {
                // No local storage for played levels present, Adding first level.
                storage.set("unlockedLevels", JSON.stringify(new Array("Wizard's Castle|0")));
            }
            var gameObjectList = entity.getEntityByComponent("gameObject");
            for (var entityId in gameObjectList)
            {
                entity.deleteEntity(entityId, true);
            }
            message.queue({type: "difficulty", value: difficulty});
            var level = map[name.trim()].level;
            soundUtil.background(level.music);
            level.setup();
        }

        function getCurrentLevelName()
        {
            var name = storage.get("level");
            if (!map[name])
            {
                // chose default level
                //noinspection AssignmentToFunctionParameterJS
                name = names[0];
            }
            return name;
        }

        function getCurrentDifficulty()
        {
            return +storage.get("difficulty") || 0;
        }

        return {
            load: load,
            loadDefault: function ()
            {
                this.load(getCurrentLevelName(), getCurrentDifficulty());
            },
            loadNext: function ()
            {
                var name = getCurrentLevelName();
                var difficulty = getCurrentDifficulty();
                var nextIndex = map[name].index + 1;
                if (nextIndex >= names.length)
                {
                    if (difficulty < 2)
                    {
                        difficulty++;
                    }
                    nextIndex = 0;
                }
                this.load(names[nextIndex], difficulty);
            },
            startMusic: function()
            {
                soundUtil.background(map[getCurrentLevelName().trim()].level.music);
            }
        };
    });