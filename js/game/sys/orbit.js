define(["entity"], function (entity)
{
    // allow each orbit to have it's own timing
    var cache = {};

    entity.createSystem("orbit", ["orbit", "position"], function (orbit, position, timing, entityId)
    {
        var centerPosition = entity.getComponentDataForEntity("position", orbit.centerId);
        cache[entityId] += timing.delta;
        var totalTime = cache[entityId];
        if (centerPosition)
        {
            position.x = centerPosition.x + orbit.amplitudeX * Math.cos((totalTime * 2 * Math.PI / orbit.periodX)
                + orbit.shiftX);
            position.y = centerPosition.y + orbit.amplitudeY * Math.cos((totalTime * 2 * Math.PI / orbit.periodY)
                + orbit.shiftY);
        }
        else
        {
            // no center position to orbit, destroy
            entity.deleteEntity(entityId);
        }
    }, function(entityId)
    {
        cache[entityId] = 0;
    }, function(entityId)
    {
        delete cache[entityId];
    });
});