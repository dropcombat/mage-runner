define(["entity"], function (entity)
{
    var cache = {};

    entity.createSystem("patrol", ["patrol", "position", "velocity"],
        function (patrol, position, velocity, timer, entityId)
        {
            var data = cache[entityId];
            var lastDelta = patrol[data.direction] - data.lastY;
            var delta = patrol[data.direction] - position.y;

            // if not the same signs (or one of them is 0), we need to move to the other patrol point
            if (lastDelta * delta <= 0)
            {
                data.direction = data.direction == "a" ? "b" : "a";
                delta = patrol[data.direction] - position.y;
            }

            velocity.y = delta > 0 ? patrol.speed : -patrol.speed;
            data.lastY = position.y;
        }, function (entityId)
        {
            var position = entity.getComponentDataForEntity("position", entityId);
            var patrol = entity.getComponentDataForEntity("patrol", entityId);
            cache[entityId] = {
                lastY: position.y,
                direction: patrol.a == position.y ? "b" : "a"
            };
        }, function (entityId)
        {
            delete cache[entityId];
        });
});
