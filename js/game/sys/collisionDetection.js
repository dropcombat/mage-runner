define(["entity", "input/sys/trigger"], function (entity, triggerSystem)
{
    var items = {};

    entity.createSystem("collisionDetection", ["position", "circleHitbox", "collidable"],
        function (position, hitbox, collidable, timer, entityId)
        {
            for (var itemId in items)
            {
                // don't hit self
                if (itemId == entityId)
                {
                    continue;
                }

                var item = items[itemId];

                // don't hit team
                if (hitbox.team && hitbox.team == item.hitbox.team)
                {
                    continue;
                }

                var maxDistance = hitbox.r + item.hitbox.r;
                var maxDistanceSquared = maxDistance * maxDistance;
                var deltaX = position.x - item.position.x;
                var deltaY = position.y - item.position.y;
                var distanceSquared = deltaX * deltaX + deltaY * deltaY;
                if (distanceSquared <= maxDistanceSquared)
                {
                    // we have a collision
                    triggerSystem.trip(collidable.trigger, itemId);
                }
            }
        });

    // keep track of all entities that have hitboxes
    entity.createSystem("circleHitboxes", ["position", "circleHitbox"], null, function (entityId)
    {
        items[entityId] = {
            position: entity.getComponentDataForEntity("position", entityId),
            hitbox: entity.getComponentDataForEntity("circleHitbox", entityId)};
    }, function (entityId)
    {
        delete items[entityId];
    });
});
