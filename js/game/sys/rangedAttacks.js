define(["entity", "game/sys/seek", "core/message"], function (entity, seekSystem, message)
{
    var chargeTime = {};

    entity.createSystem("rangedAttacks", ["position", "ranged"], function (position, ranged, timer, entityId)
    {
        var localChargeTime = ranged.chargeTime;

        var powerGrabberData = entity.getComponentDataForEntity("powerGrabber", entityId);
        if (powerGrabberData && powerGrabberData.speed)
        {
            localChargeTime /= powerGrabberData.speed.current;
        }

        chargeTime[entityId] += timer.delta;
        if (chargeTime[entityId] > localChargeTime)
        {
            chargeTime[entityId] -= localChargeTime;

            createProjectile();
            message.queue({type: "sound", id: entityId, sound: "fire"});
            if (powerGrabberData && powerGrabberData.multi && powerGrabberData.multi.current)
            {
                var upId = createProjectile();
                var downId = createProjectile();
                upId = upId[1] || upId[0];
                downId = downId[1] || downId[0];

                var data = entity.getComponentDataForEntity("velocity", upId);
                data.y = 90;
                data = entity.getComponentDataForEntity("velocity", downId);
                data.y = -90;

                entity.refreshEntity(upId);
                entity.refreshEntity(downId);
            }
        }

        //noinspection FunctionTooLongJS
        function createProjectile()
        {
            var projectileId = entity.createEntity("projectileId");
            entity.addComponentToEntity("gameObject", projectileId);
            entity.addComponentToEntity("position", projectileId);
            var data = entity.getComponentDataForEntity("position", projectileId);
            data.x = position.x + ranged.xOffset;
            data.y = position.y + ranged.yOffset;
            entity.addComponentToEntity("image", projectileId);
            data = entity.getComponentDataForEntity("image", projectileId);
            data.src = ranged.image;
            data.width = ranged.imageWidth;
            data.height = ranged.imageHeight;
            entity.addComponentToEntity("circleHitbox", projectileId);
            data = entity.getComponentDataForEntity("circleHitbox", projectileId);
            data.r = ranged.r;
            data.team = ranged.team;
            entity.addComponentToEntity("damage", projectileId);
            data = entity.getComponentDataForEntity("damage", projectileId);
            data.damage = ranged.damage;
            if (ranged.wave)
            {
                var centerId = entity.createEntity("wave projectile center");
                entity.addComponentToEntity("position", centerId);
                data = entity.getComponentDataForEntity("position", centerId);
                data.x = position.x + ranged.xOffset;
                data.y = position.y + ranged.yOffset;
                entity.addComponentToEntity("velocity", centerId);
                data = entity.getComponentDataForEntity("velocity", centerId);
                data.x = ranged.xVelocity;
                data.y = ranged.yVelocity;
                entity.refreshEntity(centerId);
                entity.addComponentToEntity("orbit", projectileId);
                data = entity.getComponentDataForEntity("orbit", projectileId);
                data.centerId = centerId;
                data.amplitudeY = ranged.wave;
                data.shiftY = -Math.PI / 2;
                data.periodY = 1300;
            }
            else
            {
                entity.addComponentToEntity("velocity", projectileId);
                data = entity.getComponentDataForEntity("velocity", projectileId);
                var homingPosition = ranged.homingTarget ? entity.getComponentDataForEntity("position",
                    ranged.homingTarget) : false;
                if (homingPosition)
                {
                    var projectilePosition = entity.getComponentDataForEntity("position", projectileId);
                    seekSystem.seekTarget(data, projectilePosition, homingPosition, ranged.homingSpeed);
                }
                else
                {
                    data.x = ranged.xVelocity;
                    data.y = ranged.yVelocity;
                }
            }
            if (ranged.onHit)
            {
                entity.addComponentToEntity("collidable", projectileId);
                data = entity.getComponentDataForEntity("collidable", projectileId);
                data.trigger = projectileId;
                entity.addComponentToEntity("trigger", projectileId);
                data = entity.getComponentDataForEntity("trigger", projectileId);
                data.on = ranged.onHit;
                data.data = ranged.onHitData;
            }
            entity.refreshEntity(projectileId);

            return centerId ? [projectileId, centerId] : [projectileId];
        }
    }, function (entityId)
    {
        chargeTime[entityId] = 0;
    });
});
