define(["entity", "game/enemy", "game/powerup", "core/message"], function (entity, enemyHelper, powerupHelper, message)
{
    var scriptedEvents = [];
    var localTime = 0;
    var index = 0;
    var isWaiting = false;

    function timestampSort(left, right)
    {
        return left.data.timestamp - right.data.timestamp;
    }

    entity.createComplexSystem("director", ["scriptedEvent"], function (timer)
    {
        if (isWaiting)
        {
            // check to see if there are no enemies so events can continue
            if (!entity.getEntityCountByComponent("enemy"))
            {
                isWaiting = false;
            }
        }

        if (!isWaiting)
        {
            localTime += timer.delta;
        }

        while (index < scriptedEvents.length && localTime >= scriptedEvents[index].data.timestamp)
        {
            var event = scriptedEvents[index].data;
            switch (event.type)
            {
            case "spawn":
                enemyHelper.spawn.apply(enemyHelper, event.params);
                break;
            case "powerup":
                powerupHelper.spawn.apply(powerupHelper, event.params);
                break;
            case "wait":
                localTime = event.timestamp;
                isWaiting = true;
                break;
            case "end":
                message.queue({type: "levelComplete"});
                break;
            default:
                console.log("unsupported scripted event type: " + event.type);
                break;
            }
            index++;
        }
    }, function (entityId)
    {
        var data = entity.getComponentDataForEntity("scriptedEvent", entityId);

        if (scriptedEvents.length === 0)
        {
            // first scriptedEvent (maybe after removing all of them)
            // reset time and states
            localTime = 0;
            isWaiting = false;
        }

        // if localTime is already passed this will move item currently pointed at by index
        // so it needs to be adjusted to look at the correct spot
        if (data.timestamp < localTime)
        {
            index++;
        }


        scriptedEvents.push({id: entityId, data: data});
        scriptedEvents.sort(timestampSort);
    }, function (entityId)
    {
        // find the entity and delete it
        for (var ndx = 0; ndx < scriptedEvents.length; ndx++)
        {
            if (scriptedEvents[ndx].id == entityId)
            {
                // if this is before the the index it needs to be adjusted
                if (index > ndx)
                {
                    index--;
                }
                scriptedEvents.splice(ndx, 1);
                // will only be one instance of this id, exit now
                return;
            }
        }
    });
});
