define(["entity"], function (entity)
{
    // resets powerups after duration runs out
    entity.createSystem("powerGrabberTimeout", ["powerGrabber"], function(powerGrabber, timer)
    {
        for(var type in powerGrabber)
        {
            var item = powerGrabber[type];
            if (item.duration)
            {
                item.duration -= timer.delta;
                if (item.duration <= 0)
                {
                    item.duration = 0;
                    item.current = item.defaultValue;
                }
            }
        }
    });
});
