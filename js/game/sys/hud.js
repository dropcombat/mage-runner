define(["entity", "game/menu"], function (entity, menuHelper)
{
    entity.createComplexSystem("hud", [], function()
    {
        menuHelper.updateSmartBombCount();
        menuHelper.updateGoldCount();
        menuHelper.updateHealthDisplay();
    }, null, null, 250);
});
