define(["entity"], function (entity)
{
    entity.createSystem("seek", ["seek", "position", "velocity"],
        function (seek, position, velocity)
        {
            var targetPosition = entity.getComponentDataForEntity("position", seek.target);
            if (targetPosition)
            {
                setModifiedVelocity(velocity, position, targetPosition, seek.speed);
            }
        }, null, null, null, 2000);

    function setModifiedVelocity(selfVelocity, selfPosition, targetPosition, speed)
    {
        // move towards the target with a contant velocity of (-200, 0) added in
        // think of it as a wind problem in physics
        // vx = -200 + cos * speed
        // vy = +/- sin * speed

        var dx = targetPosition.x - selfPosition.x;
        var dy = targetPosition.y - selfPosition.y;
        var distance = Math.sqrt(dx*dx + dy*dy);
        var radA = Math.acos(-dx / distance);
        var radB = 200 * Math.sin(radA) / speed;
        var radC = Math.PI - radA - radB;
        selfVelocity.x = -200 + Math.cos(radC) * speed;
        selfVelocity.y = Math.sin(radC) * speed * (dy > 0 ? 1 : -1);
    }

    return {
        seekTarget: setModifiedVelocity
    };
});
