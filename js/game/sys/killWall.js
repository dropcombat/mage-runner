define(["entity"], function (entity)
{
    entity.createSystem("killWall", ["position"], function(position, timer, entityId)
    {
        if (position.x < -100 || position.x > 2500)
        {
            entity.deleteEntity(entityId);
        }
    });
});
