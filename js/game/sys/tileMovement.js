define(["entity"], function (entity)
{
    entity.createSystem("tileMovement", ["shape", "tileMovement"], function(shape, tileMovement, timer)
    {
        var seconds = timer.delta / 1000;
        shape.tileOffsetX -= tileMovement.x * seconds;
        shape.tileOffsetY -= tileMovement.y * seconds;
    });
});
