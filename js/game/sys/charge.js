define(["entity"], function (entity)
{
    var cache = {};

    entity.createSystem("charge", ["charger", "position", "velocity"],
        function (charger, position, velocity, timer, entityId)
        {
            var shouldCharge = false;
            var heroes = entity.getEntityByComponent("hero");
            for (var heroId in heroes)
            {
                var heroPosition = entity.getComponentDataForEntity("position", heroId);
                if (heroPosition)
                {
                    var delta = heroPosition.y - position.y;
                    if (delta > 0 ? delta <= charger.sensorRange : delta >= -charger.sensorRange)
                    {
                        shouldCharge = true;
                        //noinspection BreakStatementJS
                        break;
                    }
                }
            }

            if (shouldCharge)
            {
                cache[entityId] = true;
                velocity.x = charger.chargeVelocity;
                velocity.y = 0;
            }
            else if (cache[entityId])
            {
                cache[entityId] = false;
                velocity.x = charger.walkVelocity;
            }

        }, function (entityId)
        {
            cache[entityId] = false;
        }, function (entityId)
        {
            delete cache[entityId];
        });
});
