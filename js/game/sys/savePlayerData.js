define(["entity", "game/hero"], function (entity, heroHelper)
{
    // save player data
    // don't need it every frame, do it a few times each second
    entity.createSystem("save", ["persistence"], function(persistence)
    {
        heroHelper.save(persistence.dataEntityId);
    }, null, null, 250);
});
