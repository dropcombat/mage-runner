define(["entity", "core/message"], function (entity, message)
{
    entity.createSystem("death", ["health"], function(health, timer, entityId)
    {
        if (health.current <= 0)
        {
            message.queue({type: "death", id: entityId});
            entity.deleteEntity(entityId);
        }
    });
});
