define(["entity", "game/hero", "core/message"], function (entity, heroHelper, message)
{
    var offScreenXSpawn = 2000;
    // can have a value of 0, 1, or 2
    var currentDifficulty = 0;

    message.addListener("mine", function (msg)
    {
        switch (msg.data.triggerType)
        {
        case "health":
            if (!entity.getComponentDataForEntity("health", msg.triggering))
            {
                return;
            }
            break;
        case "team":
            var hitbox = entity.getComponentDataForEntity("circleHitbox", msg.triggering);
            if (!hitbox || (";" + msg.data.teams + ";").split(";" + hitbox.team + ";").length < 2)
            {
                return;
            }
            break;
        default:
            return;
        }

        // trigger explosion
        var data = entity.getComponentDataForEntity("image", msg.id);
        data.src = msg.data.triggerImage;
        data = entity.getComponentDataForEntity("velocity", msg.id);
        data.x = -200;
        data.y = 0;

        entity.removeComponentFromEntity("circleHitbox", msg.id);
        entity.removeComponentFromEntity("health", msg.id);
        entity.removeComponentFromEntity("collidable", msg.id);
        entity.removeComponentFromEntity("trigger", msg.id);
        entity.removeComponentFromEntity("seek", msg.id);

        entity.addComponentToEntity("countdown", msg.id);
        data = entity.getComponentDataForEntity("countdown", msg.id);
        data.timer = 600;
        data.message = "explode";
        data.data = msg.data.countdown;
        entity.refreshEntity(msg.id);

        message.queue({type: "sound", id: msg.id, sound: "trigger"});
    });

    message.addListener("explode", function (msg)
    {
        var data = entity.getComponentDataForEntity("image", msg.id);
        var newImageData = msg.data.image;
        if (newImageData.src)
        {
            data.src = newImageData.src;
        }
        if (newImageData.width)
        {
            data.width = newImageData.width;
        }
        if (newImageData.height)
        {
            data.height = newImageData.height;
        }

        entity.addComponentToEntity("circleHitbox", msg.id);
        data = entity.getComponentDataForEntity("circleHitbox", msg.id);
        data.r = msg.data.radius;
        data.team = "explosion";
        entity.addComponentToEntity("damage", msg.id);
        data = entity.getComponentDataForEntity("damage", msg.id);
        data.damage = msg.data.damage;
        entity.addComponentToEntity("countdown", msg.id);
        data = entity.getComponentDataForEntity("countdown", msg.id);
        data.timer = 600;
        data.message = "delete";
        entity.refreshEntity(msg.id);

        message.queue({type: "sound", id: msg.id, sound: "explode"});
    });

    message.addListener("delete", function (msg)
    {
        // skip the yeild to avoid complications with systms not updating in time
        entity.deleteEntity(msg.id, true);
    });

    message.addListener("difficulty", function (msg)
    {
        currentDifficulty = msg.value;
        if (currentDifficulty !== 0 && currentDifficulty != 1 && currentDifficulty != 2)
        {
            // invalid difficulty set
            currentDifficulty = 0;
        }
    });

    //noinspection FunctionTooLongJS
    function setCommonEnemyValues(enemyEntity, y, patrolBy, healthScale, imageName)
    {
        entity.addComponentToEntity("gameObject", enemyEntity);
        entity.addComponentToEntity("enemy", enemyEntity);
        entity.addComponentToEntity("position", enemyEntity);
        var data = entity.getComponentDataForEntity("position", enemyEntity);
        data.x = offScreenXSpawn;
        data.y = y;
        entity.addComponentToEntity("velocity", enemyEntity);
        data = entity.getComponentDataForEntity("velocity", enemyEntity);
        data.x = -getMovementSpeed(0);
        entity.addComponentToEntity("image", enemyEntity);
        data = entity.getComponentDataForEntity("image", enemyEntity);
        data.src = imageName;
        entity.addComponentToEntity("circleHitbox", enemyEntity);
        data = entity.getComponentDataForEntity("circleHitbox", enemyEntity);
        data.r = 45;
        data.team = "enemy";
        entity.addComponentToEntity("health", enemyEntity);
        data = entity.getComponentDataForEntity("health", enemyEntity);
        data.max = getHealth(healthScale);
        data.current = data.max;
        entity.addComponentToEntity("collidable", enemyEntity);
        data = entity.getComponentDataForEntity("collidable", enemyEntity);
        data.trigger = enemyEntity;
        entity.addComponentToEntity("trigger", enemyEntity);
        data = entity.getComponentDataForEntity("trigger", enemyEntity);
        data.on = "damage";

        if (typeof patrolBy == "number")
        {
            entity.addComponentToEntity("patrol", enemyEntity);
            data = entity.getComponentDataForEntity("patrol", enemyEntity);
            data.a = y;
            data.b = y + patrolBy;
        }
    }

    function getDamage(scale)
    {
        return [40, 100, 400][currentDifficulty] * scale;
    }

    function getCastSpeed(scale)
    {
        return [2300, 1900, 1500][currentDifficulty] * scale;
    }

    function getHealth(scale)
    {
        return [80, 200, 800][currentDifficulty] * scale;
    }

    function getMovementSpeed(scale)
    {
        return 200 + [100, 150, 200][currentDifficulty] * scale;
    }

    return {
        spawn: function (type, y, patrolBy)
        {
            switch (type)
            {
            case "flying bomb":
                this.spawnFlyingBomb(y);
                break;
            case "rune":
                this.spawnRune(y);
                break;
            case "wizard":
                this.spawnWizard(y, patrolBy);
                break;
            case "golem":
                this.spawnGolem(y, patrolBy);
                break;
            case "golem caster":
                this.spawnGolemCaster(y, patrolBy);
                break;
            case "eye":
                this.spawnEye(y, patrolBy || 0);
                break;
            case "pit":
                this.spawnPit(y);
                break;
            case "wall":
                this.spawnWall(y);
                break;
            case "archer":
                this.spawnArcher(y, patrolBy);
                break;
            case "charger":
                this.spawnCharger(y, patrolBy);
                break;
            default:
                console.log("unsupported spawn type: " + type);
                break;
            }
        },
        spawnFlyingBomb: function (y)
        {
            var enemyEntity = entity.createEntity("enemy");
            setCommonEnemyValues(enemyEntity, y, false, 1, "flying bomb.png");

            entity.addComponentToEntity("sounds", enemyEntity);
            var soundData = entity.getComponentDataForEntity("sounds", enemyEntity);
            //soundData.trigger = "trigger";
            soundData.explode = "explosion";

            entity.addComponentToEntity("seek", enemyEntity);
            var data = entity.getComponentDataForEntity("seek", enemyEntity);
            data.target = heroHelper.getHeroId();
            data.speed = getMovementSpeed(0.5);
            entity.addComponentToEntity("trigger", enemyEntity);
            data = entity.getComponentDataForEntity("trigger", enemyEntity);
            data.on = "mine";
            data.data = {
                triggerImage: "flying bomb trigger.png",
                triggerType: "team",
                teams: "hero;explosion",
                countdown: {
                    image: {
                        src: "flying bomb explosion.png",
                        width: 180,
                        height: 180
                    },
                    radius: 90,
                    damage: getDamage(2)
                }
            };

            entity.refreshEntity(enemyEntity);
        },
        spawnRune: function (y)
        {
            var enemyEntity = entity.createEntity("enemy");
            setCommonEnemyValues(enemyEntity, y, false, 1, "runes.png");

            entity.addComponentToEntity("sounds", enemyEntity);
            var soundData = entity.getComponentDataForEntity("sounds", enemyEntity);
            soundData.explode = "explosion";

            var data = entity.getComponentDataForEntity("image", enemyEntity);
            data.width = 180;
            data.height = 180;
            data = entity.getComponentDataForEntity("circleHitbox", enemyEntity);
            data.r = 90;
            data.team = "hazard";
            entity.addComponentToEntity("trigger", enemyEntity);
            data = entity.getComponentDataForEntity("trigger", enemyEntity);
            data.on = "mine";
            data.data = {
                triggerImage: "runes trigger.png",
                triggerType: "health",
                countdown: {
                    image: {
                        src: "runes explosion.png",
                        width: 270,
                        height: 270
                    },
                    radius: 135,
                    damage: getDamage(4)
                }
            };

            entity.refreshEntity(enemyEntity);
        },
        spawnWizard: function (y, patrolBy)
        {
            var enemyEntity = entity.createEntity("enemy");
            setCommonEnemyValues(enemyEntity, y, patrolBy, 1, "wizard.png");

            entity.addComponentToEntity("sounds", enemyEntity);
            var soundData = entity.getComponentDataForEntity("sounds", enemyEntity);
            soundData.fire = "orb";

            entity.addComponentToEntity("ranged", enemyEntity);
            var data = entity.getComponentDataForEntity("ranged", enemyEntity);
            data.chargeTime = getCastSpeed(1);
            data.xOffset = -45;
            data.yOffset = -15;
            data.homingTarget = heroHelper.getHeroId();
            data.homingSpeed = getMovementSpeed(3) - 200;
            data.xVelocity = -getMovementSpeed(3);
            data.image = "orb.png";
            data.team = "enemy";
            data.r = 18;
            data.damage = getDamage(1);
            data.wave = 0;
            data.onHit = "remove";
            data.onHitData = {team: "hero"};
            entity.refreshEntity(enemyEntity);
        },
        spawnGolem: function (y, patrolBy)
        {
            var enemyEntity = entity.createEntity("enemy");
            setCommonEnemyValues(enemyEntity, y, patrolBy, 2.5, "golem.png");
            var data = entity.getComponentDataForEntity("velocity", enemyEntity);
            data.x = -getMovementSpeed(2);
            entity.addComponentToEntity("damage", enemyEntity);
            data = entity.getComponentDataForEntity("damage", enemyEntity);
            data.damage = 40;
            entity.refreshEntity(enemyEntity);
        },
        spawnGolemCaster: function (y, patrolBy)
        {
            var enemyEntity = entity.createEntity("enemy");
            setCommonEnemyValues(enemyEntity, y, patrolBy, 2.5, "golem caster.png");

            entity.addComponentToEntity("sounds", enemyEntity);
            var soundData = entity.getComponentDataForEntity("sounds", enemyEntity);
            soundData.fire = "orb";

            var data = entity.getComponentDataForEntity("velocity", enemyEntity);
            data.x = -400;
            entity.addComponentToEntity("damage", enemyEntity);
            data = entity.getComponentDataForEntity("damage", enemyEntity);
            data.damage = getDamage(1);
            entity.addComponentToEntity("ranged", enemyEntity);

            data = entity.getComponentDataForEntity("ranged", enemyEntity);
            data.chargeTime = 1500;
            data.xOffset = -45;
            data.yOffset = -15;
            data.homingTarget = heroHelper.getHeroId();
            data.homingSpeed = getMovementSpeed(3) - 200;
            data.xVelocity = -getMovementSpeed(3);
            data.image = "orb green.png";
            data.team = "enemy";
            data.r = 18;
            data.damage = getDamage(1);
            data.wave = 0;
            data.onHit = "remove";
            data.onHitData = {team: "hero"};
            entity.refreshEntity(enemyEntity);
        },
        spawnEye: function (y, periodShift)
        {
            var enemyEntity = entity.createEntity("enemy");
            setCommonEnemyValues(enemyEntity, y, false, 1, "eye.png");
            entity.removeComponentFromEntity("velocity", enemyEntity);

            var centerEntity = entity.createEntity("orbit center");
            entity.addComponentToEntity("gameObject", centerEntity);
            entity.addComponentToEntity("position", centerEntity);
            var data = entity.getComponentDataForEntity("position", centerEntity);
            data.x = offScreenXSpawn;
            data.y = y;
            entity.addComponentToEntity("velocity", centerEntity);
            data = entity.getComponentDataForEntity("velocity", centerEntity);
            data.x = -(getMovementSpeed(0)) / 2;
            entity.refreshEntity(centerEntity);

            entity.addComponentToEntity("orbit", enemyEntity);
            data = entity.getComponentDataForEntity("orbit", enemyEntity);
            data.centerId = centerEntity;
            data.amplitudeX = 100;
            data.periodX = 2500;
            data.shiftX = 2 * Math.PI * periodShift;
            data.amplitudeY = 100;
            data.periodY = 2500;
            data.shiftY = Math.PI / 2 + 2 * Math.PI * periodShift;

            entity.addComponentToEntity("ranged", enemyEntity);
            data = entity.getComponentDataForEntity("ranged", enemyEntity);
            data.chargeTime = getCastSpeed(1.3);
            data.xVelocity = -getMovementSpeed(3);
            data.image = "small eye.png";
            data.team = "enemy";
            data.r = 18;
            data.damage = getDamage(0.5);
            data.wave = 0;
            data.onHit = "remove";
            data.onHitData = {team: "hero"};
            entity.addComponentToEntity("damage", enemyEntity);
            data = entity.getComponentDataForEntity("damage", enemyEntity);
            data.damage = getDamage(0.5);

            entity.refreshEntity(enemyEntity);
        },
        spawnPit: function (y)
        {
            var enemyEntity = entity.createEntity("enemy");
            setCommonEnemyValues(enemyEntity, y, false, 1, "pit.png");

            entity.removeComponentFromEntity("health", enemyEntity);
            var data = entity.getComponentDataForEntity("circleHitbox", enemyEntity);
            data.team = "hazard";
            entity.addComponentToEntity("damage", enemyEntity);
            data = entity.getComponentDataForEntity("damage", enemyEntity);
            data.damage = getDamage(10);
            entity.refreshEntity(enemyEntity);
        },
        spawnWall: function (y)
        {
            var enemyEntity = entity.createEntity("enemy");
            setCommonEnemyValues(enemyEntity, y, false, 6, "wall.png");

            entity.addComponentToEntity("damage", enemyEntity);
            var data = entity.getComponentDataForEntity("damage", enemyEntity);
            data.damage = getDamage(0.25);
            entity.refreshEntity(enemyEntity);
        },
        spawnArcher: function (y, patrolBy)
        {
            var enemyEntity = entity.createEntity("enemy");
            setCommonEnemyValues(enemyEntity, y, patrolBy, 1, "goblin archer.png");

            entity.addComponentToEntity("ranged", enemyEntity);
            var data = entity.getComponentDataForEntity("ranged", enemyEntity);
            data.chargeTime = getCastSpeed(1);
            data.xOffset = -45;
            data.xVelocity = -getMovementSpeed(5);
            data.image = "arrow.png";
            data.imageWidth = 90;
            data.centerXPercentage = 0.2;
            data.team = "enemy";
            data.r = 18;
            data.damage = getDamage(0.75);
            data.wave = 0;
            data.onHit = "remove";
            data.onHitData = {team: "hero"};
            entity.refreshEntity(enemyEntity);
        },
        spawnCharger: function (y, patrolBy)
        {
            var enemyEntity = entity.createEntity("enemy");
            setCommonEnemyValues(enemyEntity, y, patrolBy, 1.5, "goblin charger.png");
            entity.addComponentToEntity("charger", enemyEntity);
            entity.addComponentToEntity("damage", enemyEntity);
            var data = entity.getComponentDataForEntity("damage", enemyEntity);
            data.damage = getDamage(2);

            entity.refreshEntity(enemyEntity);
        }
    };

});
