define(["entity", "input/keyListener", "core/message", "storage/local"],
    function (entity, keyListener, message, storage)
    {
        var heroSpeed = 270;
        var heroEntity;
        var projectileOffset = 45;
        var projectileSpeed = 400;
        var isAlternateFire = false;

        var persistentEntity;
        var lastGold;

        var upgradeValues = {
            "Damage":     [ 70, 100, 140, 190, 250, 320, 400, 490, 590, 700],
            "Cast Speed": [900, 850, 800, 750, 700, 650, 600, 550, 500, 450],
            "Health":     [ 70, 100, 140, 190, 250, 320, 400, 490, 590, 700],
            "Movement":   [285, 320, 355, 390, 425, 460, 495, 530, 565, 600]
        };

        // damage behavoir
        message.addListener("damage", function (msg)
        {
            var health = entity.getComponentDataForEntity("health", msg.id);
            var damage = entity.getComponentDataForEntity("damage", msg.triggering);
            if (health && damage)
            {
                health.current -= damage.damage;
            }
        });
        message.addListener("remove", function (msg)
        {
            var hitbox = entity.getComponentDataForEntity("circleHitbox", msg.triggering);
            var health = entity.getComponentDataForEntity("health", msg.triggering);
            if (hitbox && hitbox.team == msg.data.team && health)
            {
                entity.deleteEntity(msg.id);
            }
        });

        // player input
        keyListener.axis(keyListener.S, keyListener.W, function (verticalAxis)
        {
            var velocity = entity.getComponentDataForEntity("velocity", heroEntity);
            if (velocity)
            {
                velocity.y = heroSpeed * verticalAxis;
            }
        });
        keyListener.axis(keyListener.A, keyListener.D, function (horizontalAxis)
        {
            var velocity = entity.getComponentDataForEntity("velocity", heroEntity);
            if (velocity)
            {
                velocity.x = heroSpeed * horizontalAxis;
            }
        });
        keyListener.press(keyListener.COMMA, function ()
        {
            // use a smart bomb if it's avalible
            var smartBombData = entity.getComponentDataForEntity("smartBomb", heroEntity);
            if (smartBombData && smartBombData.count > 0)
            {
                message.queue({type: "sound", id: heroEntity, sound: "bomb"});
                smartBombData.count--;
                var enemyList = entity.getEntityByComponent("enemy");
                for (var enemyId in enemyList)
                {
                    if (entity.getComponentDataForEntity("health", enemyId))
                    {
                        entity.deleteEntity(enemyId);
                    }
                }
            }
        });
        keyListener.press(keyListener.PERIOD, function ()
        {
            isAlternateFire = !isAlternateFire;
            setFireMode(isAlternateFire);
        });

        function setFireMode(isAlternate)
        {
            var data = entity.getComponentDataForEntity("ranged", heroEntity);
            var sounds = entity.getComponentDataForEntity("sounds", heroEntity);
            if (isAlternate)
            {
                data.chargeTime = getUpgradeValue("Cast Speed");
                data.xOffset = projectileOffset;
                data.xVelocity = projectileSpeed * 3 / 4;
                //noinspection SpellCheckingInspection
                data.image = "waveball.png";
                data.team = "hero";
                data.r = 18;
                data.damage = getUpgradeValue("Damage") * 0.6;
                data.wave = 80;
                data.onHit = "";
                sounds.fire = "fireball";
            }
            else
            {
                data.chargeTime = getUpgradeValue("Cast Speed");
                data.xOffset = projectileOffset;
                data.xVelocity = projectileSpeed;
                data.image = "fireball.png";
                data.team = "hero";
                data.r = 18;
                data.damage = getUpgradeValue("Damage");
                data.wave = 0;
                data.onHit = "remove";
                data.onHitData = {team: "enemy"};
                sounds.fire = "fireball";
            }
        }

        function initPersistentEntity()
        {
            if (persistentEntity)
            {
                return;
            }

            persistentEntity = entity.createEntity("information across levels");
            entity.addComponentToEntity("gold", persistentEntity);
            var data = entity.getComponentDataForEntity("gold", persistentEntity);
            lastGold = storage.get("gold");
            data.current = lastGold || 0;
        }

        function getUpgradeValue(type)
        {
            return upgradeValues[type][heroHelper.getUpgradeLevel(type) - 1];
        }

        var heroHelper =  {
            spawn: function ()
            {
                initPersistentEntity();
                heroSpeed = getUpgradeValue("Movement");

                heroEntity = entity.createEntity("hero");
                entity.addComponentToEntity("gameObject", heroEntity);
                entity.addComponentToEntity("hero", heroEntity);
                entity.addComponentToEntity("powerGrabber", heroEntity);
                entity.addComponentToEntity("smartBomb", heroEntity);
                entity.addComponentToEntity("sounds", heroEntity);
                var sounds = entity.getComponentDataForEntity("sounds", heroEntity);
                sounds.bomb = "bomb";
                entity.addComponentToEntity("persistence", heroEntity);
                var data = entity.getComponentDataForEntity("persistence", heroEntity);
                data.dataEntityId = persistentEntity;
                entity.addComponentToEntity("position", heroEntity);
                data = entity.getComponentDataForEntity("position", heroEntity);
                data.x = 0;
                data.y = 0;
                entity.addComponentToEntity("velocity", heroEntity);
                entity.addComponentToEntity("image", heroEntity);
                data = entity.getComponentDataForEntity("image", heroEntity);
                data.src = "hero.png";
                entity.addComponentToEntity("health", heroEntity);
                data = entity.getComponentDataForEntity("health", heroEntity);
                data.max = getUpgradeValue("Health");
                data.current = data.max;
                entity.addComponentToEntity("circleHitbox", heroEntity);
                data = entity.getComponentDataForEntity("circleHitbox", heroEntity);
                data.r = 45;
                data.team = "hero";
                entity.addComponentToEntity("collidable", heroEntity);
                data = entity.getComponentDataForEntity("collidable", heroEntity);
                data.trigger = heroEntity;
                entity.addComponentToEntity("trigger", heroEntity);
                data = entity.getComponentDataForEntity("trigger", heroEntity);
                data.on = "damage";
                entity.addComponentToEntity("ranged", heroEntity);
                entity.addComponentToEntity("restrictedPosition", heroEntity);
                data = entity.getComponentDataForEntity("restrictedPosition", heroEntity);
                data.minX = 0;
                data.maxX = 540;
                data.minY = -405;
                data.maxY = 405;
                entity.refreshEntity(heroEntity);

                setFireMode();
            },
            getHeroId: function ()
            {
                return heroEntity;
            },
            getPersistentDataId: function ()
            {
                initPersistentEntity();
                return persistentEntity;
            },
            save: function (entityId)
            {
                // only support saving one persistant entity right now
                if (entityId != persistentEntity)
                {
                    console.log("unsupported save call with entityId: " + entityId);
                }

                var goldData = entity.getComponentDataForEntity("gold", entityId);
                if (lastGold != goldData.current)
                {
                    lastGold = goldData.current;
                    storage.set("gold", lastGold);
                }
            },
            getUpgradeLevel: function(type)
            {
                return storage.get("upgrade" + type) || 1;
            },
            setUpgradeLevel: function(type, level)
            {
                storage.set("upgrade" + type, level);
            }
        };

        return heroHelper;

    });
