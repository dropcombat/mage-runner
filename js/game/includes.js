// include all core components and systems
define(
    ["game/c/charger", "game/c/circleHitbox", "game/c/collidable", "game/c/damage", "game/c/enemy", "game/c/gameObject",
     "game/c/gold", "game/c/health", "game/c/hero", "game/c/orbit", "game/c/patrol", "game/c/persistence",
     "game/c/powerGrabber", "game/c/ranged", "game/c/scriptedEvent", "game/c/seek", "game/c/smartBomb",
     "game/c/tileMovement", "game/sys/charge", "game/sys/collisionDetection", "game/sys/death", "game/sys/director",
     "game/sys/hud", "game/sys/killWall", "game/sys/orbit", "game/sys/patrol", "game/sys/powerGrabberTimeout",
     "game/sys/rangedAttacks", "game/sys/savePlayerData", "game/sys/seek", "game/sys/tileMovement"]);