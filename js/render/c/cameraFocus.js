define(["entity"], function (entity)
{
    entity.createComponent("cameraFocus", "allow camera to focus on this object, there should be only 1 at a time",
        "cameraFocus", function ()
        {
            return {};
        });
});
