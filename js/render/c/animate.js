define(["entity"], function (entity)
{

    entity.createComponent("animate", "information to animate an image component", "animate", function ()
    {
        //noinspection JSUnusedGlobalSymbols
        return {
            pose: "idle",
            offset: 36,
            poses: {
                idle: {
                    img: "enemy",
                    count: 5,
                    length: 5
                }
            }
        };
    });
});
