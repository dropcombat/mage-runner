define(["entity"], function (entity)
{
    entity.createComponent("shape", "shape to render", "shape", function ()
    {
        return {
            width: 90,
            height: 90,
            fill: "red",
            stroke: "black",
            strokeWidth: 4,
            tile: "",
            tileOffsetX: 0,
            tileOffsetY: 0,
            centerXPercentage: 0.5,
            centerYPercentage: 0.5,
            zIndex: 10,
            opacity: 1
        };
    });
});
