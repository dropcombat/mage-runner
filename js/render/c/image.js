define(["entity"], function (entity)
{
    entity.createComponent("image", "image to render", "image", function ()
    {
        return {
            src: "",
            width: 90,
            height: 90,
            widthScalier: 1,
            heightScalier: 1,
            cropX: 0,
            cropY: 0,
            centerXPercentage: 0.5,
            centerYPercentage: 0.5,
            zIndex: 10
        };
    });
});
