define(["entity"], function (entity)
{
    entity.createComponent("text", "text to render", "text", function ()
    {
        return {
            width: 90,
            text: "text",
            fontSize: 30,
            fill: "black",
            centerXPercentage: 0.5,
            centerYPercentage: 0.5,
            zIndex: 11
        };
    });
});
