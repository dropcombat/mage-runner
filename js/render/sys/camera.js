define(["entity"], function (entity)
{
    // default values can be overwritten through setScreenSize
    var screenWidth = 1366;
    var screenHeight = 768;

    var cameraX;
    var cameraY;
    var cameraZoom = 1;

    // call centerCamera on (0,0) to ensure that the view values get populated before being used
    centerCamera({
        x: 0,
        y: 0
    });

    // center on an object
    function centerCamera(position)
    {
        cameraX = position.x;
        cameraY = position.y;
    }

    // Looks for a camera focus component, so we only have 1 thing at a time the camera can focus on.
    entity.createSystem("centerCamera", ["position", "cameraFocus"], centerCamera, function (entityId)
    {
        centerCamera(entity.getComponentDataForEntity("position", entityId));
    });

    //noinspection JSUnusedGlobalSymbols
    return {
        setScreenSize: function (width, height)
        {
            screenWidth = width;
            screenHeight = height;
        },
        screenToWorldX: function (screenX)
        {
            return (screenX - (screenWidth / 2)) / cameraZoom + cameraX;
        },
        screenToWorldY: function (screenY)
        {
            return ((screenHeight / 2) - screenY) / cameraZoom + cameraY;
        },
        worldToScreenX: function (worldX)
        {
            return (worldX - cameraX) * cameraZoom + (screenWidth / 2);

        },
        worldToScreenY: function (worldY)
        {
            return (screenHeight / 2) - (worldY - cameraY) * cameraZoom;
        },
        worldToScreenWidth: function (worldWidth)
        {
            return worldWidth * cameraZoom;
        },
        worldToScreenHeight: function (worldHeight)
        {
            return worldHeight * cameraZoom;
        }
    };
});
