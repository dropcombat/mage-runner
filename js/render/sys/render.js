define(["entity", "Kinetic", "render/sys/camera", "domReady!"], function (entity, Kinetic, camera)
{
    // wait for domReady because container is used to find a dom element

    // defaults to MS Surface size
    var targetWidth = 1920;
    var targetHeight = 1080;
    var scale = 1;

    var imageCache = {};
    var shapeCache = {};
    var textCache = {};
    var imageData = {};

    var stage = new Kinetic.Stage({
        container: "container",
        width: targetWidth,
        height: targetHeight,
        scale: {x: scale, y: scale}
    });
    var layer = new Kinetic.Layer();
    stage.add(layer);

    window.addEventListener("resize", resize, false);

    function resize()
    {
        var widthScale = window.innerWidth / targetWidth;
        var heightScale = window.innerHeight / targetHeight;
        scale = Math.min(widthScale, heightScale);
        if (scale > 1)
        {
            scale = 1;
        }
        var width = targetWidth * scale;
        var height = targetHeight * scale;
        stage.setSize(width, height);
        stage.setScale({x: scale, y: scale});
    }

    function getImageData(name)
    {
        var src = "resources/i/" + name;
        if (!imageData[src])
        {
            var img = new Image();

            var data = {
                loaded: false,
                src: src,
                img: img
            };

            img.onload = function ()
            {
                data.loaded = true;
                data.width = img.width;
                data.height = img.height;
                // clean up self reference to allow for garbage collection in IE
                img.onload = null;
            };
            img.src = src;
            imageData[src] = data;
        }

        return imageData[src];
    }

    // render everything every frame
    entity.createComplexSystem("render", [], function (timer)
    {
        entity.updateSystem("renderShapes", timer);
        entity.updateSystem("renderImages", timer);
        entity.updateSystem("renderText", timer);
        layer.draw();
    });

    // region update images as needed
    entity.createComplexSystem("renderImages", ["position", "image"], function (timer, entityList)
    {
        for (var ndx = 0; ndx < entityList.length; ndx++)
        {
            var entityId = entityList[ndx];
            var imageComponentData = entity.getComponentDataForEntity("image", entityId);
            var imageData = getImageData(imageComponentData.src);
            var cache = imageCache[entityId];
            if (!imageData.loaded)
            {
                if (cache)
                {
                    cache.imgObject.remove();
                    delete imageCache[entityId];
                }
                continue;
            }
            if (!cache || cache.src != imageData.src)
            {
                if (cache)
                {
                    cache.imgObject.setImage(imageData.img);
                }
                else
                {
                    cache = {crop: {}};
                    imageCache[entityId] = cache;
                    cache.imgObject = new Kinetic.Image({
                        image: imageData.img
                    });
                    layer.add(cache.imgObject);
                }
                cache.src = imageData.src;
            }

            var forceCrop = false;
            var renderData = getRenderData(entity.getComponentDataForEntity("position", entityId), imageComponentData);
            if (cache.left != renderData.left)
            {
                cache.imgObject.setX(renderData.left);
                cache.left = renderData.left;
            }
            if (cache.top != renderData.top)
            {
                cache.imgObject.setY(renderData.top);
                cache.top = renderData.top;
            }
            if (cache.width != renderData.width)
            {
                cache.imgObject.setWidth(renderData.width);
                cache.width = renderData.width;
                forceCrop = true;
            }
            if (cache.height != renderData.height)
            {
                cache.imgObject.setHeight(renderData.height);
                cache.height = renderData.height;
                forceCrop = true;
            }
            if (cache.zIndex != imageComponentData.zIndex)
            {
                cache.imgObject.setZIndex(imageComponentData.zIndex);
                cache.zIndex = imageComponentData.zIndex;
            }
            //noinspection OverlyComplexBooleanExpressionJS
            if (forceCrop || cache.crop.x != imageComponentData.cropX || cache.crop.y != imageComponentData.cropY
                    || cache.widthScalier != imageComponentData.widthScalier || cache.heightScalier
                    != imageComponentData.heightScalier)
            {
                cache.imgObject.setCrop({
                    x: imageComponentData.cropX,
                    y: imageComponentData.cropY,
                    width: imageComponentData.width / imageComponentData.widthScalier,
                    height: imageComponentData.height / imageComponentData.heightScalier
                });
                cache.crop.x = imageComponentData.cropX;
                cache.crop.y = imageComponentData.cropY;
                cache.widthScalier = imageComponentData.widthScalier;
                cache.heightScalier = imageComponentData.heightScalier;
            }
        }
    }, function (entityId)
    {
        var imageComponentData = entity.getComponentDataForEntity("image", entityId);
        getImageData(imageComponentData.src);
    }, function (entityId)
    {
        var cache = imageCache[entityId];
        if (cache)
        {
            cache.imgObject.remove();
        }
        delete imageCache[entityId];
    });
    // endregion

    // region update shapes as needed
    entity.createComplexSystem("renderShapes", ["position", "shape"], function (timer, entityList)
    {
        for (var ndx = 0; ndx < entityList.length; ndx++)
        {
            var entityId = entityList[ndx];
            var shapeComponentData = entity.getComponentDataForEntity("shape", entityId);
            var cache = shapeCache[entityId];

            if (shapeComponentData.tile)
            {
                var tileData = getImageData(shapeComponentData.tile);
                if (tileData.loaded && cache.tile != shapeComponentData.tile)
                {
                    // add currently loaded tile
                    cache.rectObject.setFillPatternImage(tileData.img);
                    cache.tile = shapeComponentData.tile;
                }
                if (cache.tileOffsetX != shapeComponentData.tileOffsetX || cache.tileOffsetY
                    != shapeComponentData.tileOffsetY)
                {
                    cache.rectObject.setFillPatternOffset(
                        [shapeComponentData.tileOffsetX, shapeComponentData.tileOffsetY]);
                    cache.tileOffsetX = shapeComponentData.tileOffsetX;
                    cache.tileOffsetY = shapeComponentData.tileOffsetY;
                }
            }
            else if (cache.tile)
            {
                // remove tile
                cache.rectObject.setFillPatternImage("");
                cache.tile = shapeComponentData.tile;
            }

            var renderData = getRenderData(entity.getComponentDataForEntity("position", entityId), shapeComponentData);
            if (cache.left != renderData.left)
            {
                cache.rectObject.setX(renderData.left);
                cache.left = renderData.left;
            }
            if (cache.top != renderData.top)
            {
                cache.rectObject.setY(renderData.top);
                cache.top = renderData.top;
            }
            if (cache.width != renderData.width)
            {
                cache.rectObject.setWidth(renderData.width);
                cache.width = renderData.width;
            }
            if (cache.height != renderData.height)
            {
                cache.rectObject.setHeight(renderData.height);
                cache.height = renderData.height;
            }
            if (cache.fill != shapeComponentData.fill)
            {
                cache.rectObject.setFill(shapeComponentData.fill);
                cache.fill = shapeComponentData.fill;
            }
            if (cache.stroke != shapeComponentData.stroke)
            {
                cache.rectObject.setStroke(shapeComponentData.stroke);
                cache.stroke = shapeComponentData.stroke;
            }
            if (cache.strokeWidth != shapeComponentData.strokeWidth)
            {
                cache.rectObject.setStrokeWidth(shapeComponentData.strokeWidth);
                cache.strokeWidth = shapeComponentData.strokeWidth;
            }
            if (cache.zIndex != shapeComponentData.zIndex)
            {
                cache.rectObject.setZIndex(shapeComponentData.zIndex);
                cache.zIndex = shapeComponentData.zIndex;
            }
            if (cache.opacity != shapeComponentData.opacity)
            {
                cache.rectObject.setOpacity(shapeComponentData.opacity);
                cache.opacity = shapeComponentData.opacity;
            }
            if (cache.cornerRadius != shapeComponentData.cornerRadius)
            {
                cache.rectObject.setCornerRadius(shapeComponentData.cornerRadius);
                cache.cornerRadius = shapeComponentData.cornerRadius;
            }
        }
    }, function (entityId)
    {
        var shapeComponentData = entity.getComponentDataForEntity("shape", entityId);
        if (shapeComponentData.tile)
        {
            getImageData(shapeComponentData.tile);
        }
        var cache = {};
        shapeCache[entityId] = cache;
        cache.rectObject = new Kinetic.Rect({});
        layer.add(cache.rectObject);
    }, function (entityId)
    {
        var cache = shapeCache[entityId];
        cache.rectObject.remove();
        delete shapeCache[entityId];
    });
    // endregion

    // region update text as needed
    entity.createComplexSystem("renderText", ["position", "text"], function (timer, entityList)
    {
        for (var ndx = 0; ndx < entityList.length; ndx++)
        {
            var entityId = entityList[ndx];
            var textComponentData = entity.getComponentDataForEntity("text", entityId);
            var cache = textCache[entityId];

            var renderData = getRenderData(entity.getComponentDataForEntity("position", entityId), textComponentData);
            if (cache.left != renderData.left)
            {
                cache.textObject.setX(renderData.left);
                cache.left = renderData.left;
            }
            if (cache.top != renderData.top)
            {
                cache.textObject.setY(renderData.top);
                cache.top = renderData.top;
            }
            if (cache.width != renderData.width)
            {
                cache.textObject.setWidth(renderData.width);
                cache.width = renderData.width;
            }
            if (cache.fontSize != textComponentData.fontSize)
            {
                cache.textObject.setFontSize(textComponentData.fontSize);
                cache.fontSize = textComponentData.fontSize;
            }
            if (cache.fill != textComponentData.fill)
            {
                cache.textObject.setFill(textComponentData.fill);
                cache.fill = textComponentData.fill;
            }
            if (cache.text != textComponentData.text)
            {
                cache.textObject.setText(textComponentData.text);
                cache.text = textComponentData.text;
            }
            if (cache.zIndex != textComponentData.zIndex)
            {
                cache.textObject.setZIndex(textComponentData.zIndex);
                cache.zIndex = textComponentData.zIndex;
            }
        }
    }, function (entityId)
    {
        var cache = {};
        textCache[entityId] = cache;
        cache.textObject = new Kinetic.Text({});
        layer.add(cache.textObject);
    }, function (entityId)
    {
        var cache = textCache[entityId];
        cache.textObject.remove();
        delete textCache[entityId];
    });
    // endregion

    function getRenderData(position, shape)
    {
        var width = camera.worldToScreenWidth(shape.width);
        // shape.height may not be defined (for text)
        var height = camera.worldToScreenHeight((shape.height || 0));
        var x = camera.worldToScreenX(position.x);
        var y = camera.worldToScreenY(position.y);
        //noinspection NonShortCircuitBooleanExpressionJS
        return {
            left: (x - width * shape.centerXPercentage) + 0.5 | 0,
            top: (y - height * shape.centerYPercentage) + 0.5 | 0,
            width: width,
            height: height
        };
    }

    //noinspection JSUnusedGlobalSymbols
    return {
        init: function (width, height)
        {
            targetWidth = width;
            targetHeight = height;
            camera.setScreenSize(width, height);
            resize();
        },
        getScale: function ()
        {
            return scale;
        },
        preloadImage: getImageData
    };

});
