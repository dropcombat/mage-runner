define(["entity"], function (entity)
{

    var cache = {};

    entity.createSystem("animation", ["animate", "image"], function (animate, image, timing, entityId)
    {
        // TODO: use timing to detect skipped frames
        var cached = cache[entityId];
        var pose = animate.pose;
        var poseInfo = animate.poses[pose];
        if (cached.pose == pose)
        {
            cached.frame = (cached.frame + 1) % (poseInfo.length * poseInfo.count);
        }
        else
        {
            //noinspection JSUnusedGlobalSymbols
            cached.pose = pose;
            cached.frame = 0;
        }

        //noinspection NonShortCircuitBooleanExpressionJS
        image.cropX = (cached.frame / poseInfo.count | 0) * animate.offset;


    }, function (entityId)
    {
        var animate = entity.getComponentDataForEntity("animate", entityId);
        //noinspection JSUnusedGlobalSymbols
        cache[entityId] = {
            pose: animate.pose,
            frame: 0
        };

    }, function (entityId)
    {
        delete cache[entityId];
    });
});
