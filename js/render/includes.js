// include all core components and systems
define(
    ["core/includes", "render/c/animate", "render/c/image", "render/c/cameraFocus", "render/c/shape", "render/c/text",
     "render/sys/animation", "render/sys/camera", "render/sys/render"]);