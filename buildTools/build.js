({
    baseUrl: "../js",
    paths: {
        "domReady": "../lib/domReady",
        "entity": "core/entity",
        "Kinetic": "../lib/kinetic-v5.1.0.min"
    },
    name: "config",
    out: "../release/game-min.js"
})
