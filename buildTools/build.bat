REM -- delete release folder
rd /s /q ..\release

REM -- copy statics
xcopy /s ..\statics ..\release\

REM -- copy require
copy ..\lib\require.js ..\release\require.js

REM -- copy resoureces
xcopy /s ..\resources ..\release\resources\

REM -- build js file
node r.js -o build.js

pause